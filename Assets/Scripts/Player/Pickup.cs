﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pickup : MonoBehaviour
{
    // Player //-----------------------------------------------

    public NavMeshAgent Owner;
   

    // Inventory //-----------------------------------------------

    [Header("Inventory")]
    public List<GameObject> inYourHands;
    public List<Transform> HandPos;
    public GameObject Hands;
    public List<GameObject> SlotTyps;

    // Pickup //-----------------------------------------------

    [Header("Pickup")]
    public bool Carry = false;
    public bool inRange;

    [Range(min: 1, max: 15)]
    public float PickupDist;
    float dist;



    [Range(min: 1, max: 15)]
    public float DropDist;
    [HideInInspector]
    public Vector3 DropPos;

    public Transform RockPos;
    Transform TargetHoldPos;


    [HideInInspector]
    public int CarryHandCurrentAmount = 0;

    [HideInInspector]
    public GameObject PickUpObject;

    // Update is called once per frame




    private void FixedUpdate()
    {

        Ray rayH = new Ray(Owner.transform.position, Owner.transform.forward);

        Debug.DrawLine(Owner.transform.position, rayH.GetPoint(DropDist), Color.red);
        Ray rayV = new Ray(rayH.GetPoint(DropDist), Vector3.down);

        Debug.DrawRay(rayH.GetPoint(DropDist), Vector3.down, Color.red);


        RaycastHit hitV;

        if (Physics.Raycast(rayV, out hitV))
        {
            DropPos = new Vector3(hitV.point.x, hitV.point.y + 3, hitV.point.z);


        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(DropPos, 1);
        Gizmos.DrawSphere(DropPos, 0.3f);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, PickupDist);

    }

    void Update()
    {
        PickUpDetection();

        Owner.GetComponent<NavMeshAgent>();

        if (inYourHands.Count == 0)
        {
            Carry = false;
        }
        else
        {
            Carry = true;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (Carry)
            {
                Drop();
            }
            else
            {
                if (inRange)
                {
                    PickUp();
                }
            }
        }
    }

    void PickUpDetection()
    {
        GetComponent<PickUpDetection>().ReactionCheck();
        //Debug.Log(PickUpObject);

        if (GetComponent<PickUpDetection>().NPCInRange.Count != 0)
        {
            inRange = true;
        }
        else
        {
            inRange = false;
        }
    }


    public void PickUp()
    {
        if (PickUpObject != null) {

            if (inYourHands.Count == 0) {

                if (PickUpObject.GetComponent<Resource>().resourceTyp == Resource.ResourceTyp.Turnip)
                {
                    TargetHoldPos = SlotTyps[0].transform;
                }
                else if (PickUpObject.GetComponent<Resource>().resourceTyp == Resource.ResourceTyp.Axe)
                {
                    TargetHoldPos = SlotTyps[1].transform;
                }
                else if (PickUpObject.GetComponent<Resource>().resourceTyp == Resource.ResourceTyp.Statue)
                {
                    TargetHoldPos = SlotTyps[2].transform;
                }

                else if (PickUpObject.GetComponent<Resource>().resourceTyp == Resource.ResourceTyp.Ston)
                {
                    TargetHoldPos = SlotTyps[3].transform;
                }
                else if (PickUpObject.GetComponent<Resource>().resourceTyp == Resource.ResourceTyp.Wood)
                {
                    TargetHoldPos = SlotTyps[4].transform;
                }

                else if (PickUpObject.GetComponent<Resource>().resourceTyp == Resource.ResourceTyp.RocketPart)
                {
                    TargetHoldPos = SlotTyps[5].transform;
                }

                else if (PickUpObject.GetComponent<Resource>().resourceTyp == Resource.ResourceTyp.Wing)
                {
                    TargetHoldPos = SlotTyps[6].transform;
                }

                if (gameObject.GetComponent<AudioHandler>()!=null){
                    gameObject.GetComponent<AudioHandler>().PickUp();
                } 
                PickUpObject.transform.rotation = new Quaternion(TargetHoldPos.transform.rotation.x, TargetHoldPos.transform.rotation.y, TargetHoldPos.transform.rotation.z, TargetHoldPos.transform.rotation.w);
                PickUpObject.transform.position = new Vector3(TargetHoldPos.transform.position.x, TargetHoldPos.transform.position.y, TargetHoldPos.transform.position.z);
                PickUpObject.transform.SetParent(TargetHoldPos.transform);
                PickUpObject.GetComponent<Rigidbody>().isKinematic = true;
                PickUpObject.GetComponent<SphereCollider>().enabled = false;

                    if (PickUpObject.GetComponent<BoxCollider>())
                    {
                    PickUpObject.GetComponent<BoxCollider>().enabled = false;
                    }

                    if (PickUpObject.GetComponent<NavMeshObstacle>())
                    {
                    PickUpObject.GetComponent<NavMeshObstacle>().enabled = false;
                    }
                    // Object.enabled = false;

                    inYourHands.Add(PickUpObject.gameObject);
                    CarryHandCurrentAmount++;
                    //Play Pickup Audio if this GameObject has an Audio Handler
                
            }
        }
    }

    public void Drop()
    {

        if (inYourHands.Count != 0) {
            inYourHands[0].transform.position = DropPos;
            inYourHands[0].transform.parent = null;
            inYourHands[0].GetComponent<Rigidbody>().isKinematic = false;
            inYourHands[0].GetComponent<SphereCollider>().enabled = true;

            if (PickUpObject.GetComponent<BoxCollider>())
            {
                PickUpObject.GetComponent<BoxCollider>().enabled = true;
            }
            if (PickUpObject.GetComponent<NavMeshObstacle>())
            {
                PickUpObject.GetComponent<NavMeshObstacle>().enabled = true;
            }
            inYourHands.Remove(inYourHands[0]);
            PickUpObject = null;
            //Debug.Log(inYourHands.Count);
            CarryHandCurrentAmount--;
            //Play Drop Sound if this Object has an Audio Handler
            if(gameObject.GetComponent<AudioHandler>()!=null){
                    gameObject.GetComponent<AudioHandler>().DropIt();
                } 
        }
    }
 


}


