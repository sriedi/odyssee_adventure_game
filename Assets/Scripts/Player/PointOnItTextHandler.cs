﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PointOnItTextHandler : MonoBehaviour
{
    
    [SerializeField]
    List<Symbol> PointOnIt;
    [SerializeField]
    UnityEvent onPoint;

    public List<Symbol> pointOnIt(){
        onPoint.Invoke();
        return PointOnIt;
    }


}
