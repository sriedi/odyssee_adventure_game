﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Object_Interaction : MonoBehaviour
{
    // Player //------------------------------------------------------------------------------

    [Header("Player")]
    public GameObject Player;
    NPCDialoge NPCD;

     public GameObject DC;

    // Detection //---------------------------------------------------------------------------

    [Header("Detection")]
    public GameObject DetectetObject;
    public GameObject SelectedObject;
    public GameObject SelectedCurrent;
    public GameObject SeletctObject;
    public GameObject Arrow;
    GameObject CArrow;
    public string CT;

    //  Selection  //-------------------------------------------------------------------------
    [Header("Selection")]
    public Material SelectMaterial;
    public Material DefaltMaterial;
    public bool Select;

    MeshFilter meshFilter;
    MeshRenderer meshRenderer;
    public Mesh SelectMesh;
    bool NoMeshRenderer;
    //  Menu  //------------------------------------------------------------------------------

    [Header("Menu")]
    public GameObject Menu;
    public GameObject MenuActive;
    public bool MenueIsActive;
    [HideInInspector]
    public Vector3 MenuPos;

    //  Materials //------------------------------------------------------------------------------


    void Update()
    {

        DetectetObject = Player.GetComponent<PlayerNavMesh>().HitObjcet;

        NPCD = Player.GetComponent<PlayerNavMesh>().NPCD;

        if (CT != null)
        {
            CT = Player.GetComponent<PlayerNavMesh>().CT;
        }

       //Detection();
       Selection();
        // Debug.Log(InRange);

    }

    void Selection()
    {


        if (DetectetObject != null)
        {
            SelectedCurrent = DetectetObject;

            if (SelectedCurrent != null && SelectedCurrent.GetComponent<Interactable>())
            {
                if (SelectedCurrent.GetComponent<MeshRenderer>())
                {

                    if (SelectedCurrent.GetComponent<MeshRenderer>().material.HasProperty("_Boolean_Wind"))
                    {

                        SelectedCurrent.GetComponent<MeshRenderer>().material.SetInt("_Boolean_Wind", 0);
                    }

                }




                if (SeletctObject == null && CArrow == null)
                {

                    if (SelectedCurrent.CompareTag("Drone"))
                    {
                        CArrow = Instantiate(Arrow, new Vector3(DetectetObject.transform.position.x, DetectetObject.transform.position.y + DetectetObject.transform.localScale.y + 2f, DetectetObject.transform.position.z), Quaternion.identity, DetectetObject.transform);
                        SelectedObject.GetComponent<MeshFilter>().mesh = SelectMesh;
                        SeletctObject = Instantiate(SelectedObject, new Vector3(DetectetObject.GetComponentInChildren<Animator>().gameObject.transform.position.x, DetectetObject.GetComponentInChildren<Animator>().gameObject.transform.position.y + 0.5f, DetectetObject.GetComponentInChildren<Animator>().gameObject.transform.position.z), DetectetObject.transform.rotation, DetectetObject.transform);
                        // CArrow = Instantiate(Arrow, new Vector3(DetectetObject.transform.position.x, DetectetObject.transform.position.y + DetectetObject.transform.localScale.y + 1f, DetectetObject.transform.position.z), Quaternion.identity, DetectetObject.transform);

                    }
                    else if (SelectedCurrent.CompareTag("Player"))
                    {
                        CArrow = Instantiate(Arrow, new Vector3(DetectetObject.transform.position.x, DetectetObject.transform.position.y + DetectetObject.transform.localScale.y + 2f, DetectetObject.transform.position.z), Quaternion.identity, DetectetObject.transform);
                        SelectedObject.GetComponent<MeshFilter>().mesh = SelectMesh;
                        SeletctObject = Instantiate(SelectedObject, new Vector3(DetectetObject.GetComponentInChildren<Animator>().gameObject.transform.position.x, DetectetObject.GetComponentInChildren<Animator>().gameObject.transform.position.y + 1.2f, DetectetObject.GetComponentInChildren<Animator>().gameObject.transform.position.z), DetectetObject.transform.rotation, DetectetObject.transform);

                    }

                    else if (SelectedCurrent.CompareTag("NPC"))
                    {
                        CArrow = Instantiate(Arrow, new Vector3(DetectetObject.transform.position.x, DetectetObject.transform.position.y + DetectetObject.transform.localScale.y + 2f, DetectetObject.transform.position.z), Quaternion.identity, DetectetObject.transform);
                        SelectedObject.GetComponent<MeshFilter>().mesh = SelectMesh;
                        SeletctObject = Instantiate(SelectedObject, new Vector3(DetectetObject.GetComponentInChildren<Animator>().gameObject.transform.position.x, DetectetObject.GetComponentInChildren<Animator>().gameObject.transform.position.y + 1.2f, DetectetObject.GetComponentInChildren<Animator>().gameObject.transform.position.z), DetectetObject.transform.rotation, DetectetObject.transform);

                    }
                    else if (SelectedCurrent.CompareTag("Resource"))
                    {

                        if (SelectedCurrent.GetComponent<Resource>().resourceTyp == Resource.ResourceTyp.Axe)
                        {
                            SelectedObject.GetComponent<MeshFilter>().sharedMesh = SelectMesh;
                        }
                        else
                        {
                            SelectedObject.GetComponent<MeshFilter>().sharedMesh = SelectedCurrent.GetComponent<MeshFilter>().sharedMesh;
                        }

                        CArrow = Instantiate(Arrow, new Vector3(DetectetObject.transform.position.x, DetectetObject.transform.position.y + DetectetObject.transform.localScale.y + 2f, DetectetObject.transform.position.z), Quaternion.identity, DetectetObject.transform);
                        SeletctObject = Instantiate(SelectedObject, new Vector3(DetectetObject.transform.position.x, DetectetObject.gameObject.transform.position.y, DetectetObject.transform.position.z), DetectetObject.transform.rotation, DetectetObject.transform);

                    }

                    else
                    {
                        if (SelectedCurrent.GetComponent<MeshFilter>())
                        {
                            CArrow = Instantiate(Arrow, new Vector3(DetectetObject.transform.position.x, DetectetObject.transform.position.y + DetectetObject.transform.localScale.y * 6f, DetectetObject.transform.position.z), Quaternion.identity, DetectetObject.transform);
                            SelectedObject.GetComponent<MeshFilter>().mesh = SelectMesh;
                            SeletctObject = Instantiate(SelectedObject, DetectetObject.transform.localPosition, Quaternion.identity, DetectetObject.transform);
                        }
                    }


                    SeletctObject.transform.SetParent(DetectetObject.transform);

                }
                else
                {

                    if (SelectedCurrent.GetComponent<MeshFilter>())
                    {

                        SeletctObject.GetComponent<MeshFilter>().sharedMesh = SelectedCurrent.GetComponent<MeshFilter>().sharedMesh;

                        SeletctObject.transform.position = SelectedCurrent.transform.position;


                    }

                    CArrow.transform.position = new Vector3(SelectedCurrent.transform.position.x, SelectedCurrent.transform.position.y + SelectedCurrent.transform.localScale.y * 6f, SelectedCurrent.transform.position.z);

                }



                SeletctObject.transform.rotation = SelectedCurrent.transform.rotation;
                SeletctObject.transform.SetParent(SelectedCurrent.transform);



                if (SelectedCurrent.CompareTag("Drone"))
                {
                    CArrow.transform.position = new Vector3(SelectedCurrent.transform.position.x, SelectedCurrent.transform.position.y + SelectedCurrent.transform.localScale.y + 4f, SelectedCurrent.transform.position.z);
                    SeletctObject.transform.localScale = SelectedCurrent.transform.localScale;
                    SeletctObject.transform.position = new Vector3(SelectedCurrent.GetComponentInChildren<Animator>().gameObject.transform.position.x, SelectedCurrent.GetComponentInChildren<Animator>().gameObject.transform.position.y + 0.5f, SelectedCurrent.GetComponentInChildren<Animator>().gameObject.transform.position.z);
                }

                if (SelectedCurrent.CompareTag("Player"))
                {
                    CArrow.transform.position = new Vector3(SelectedCurrent.transform.position.x, SelectedCurrent.transform.position.y + SelectedCurrent.transform.localScale.y + 2f, SelectedCurrent.transform.position.z);
                    SeletctObject.transform.localScale = SelectedCurrent.transform.localScale;
                    SeletctObject.transform.position = new Vector3(SelectedCurrent.GetComponentInChildren<Animator>().gameObject.transform.position.x, SelectedCurrent.GetComponentInChildren<Animator>().gameObject.transform.position.y + 1.2f, SelectedCurrent.GetComponentInChildren<Animator>().gameObject.transform.position.z);

                }

                else if (SelectedCurrent.CompareTag("NPC"))
                {
                    CArrow.transform.position = new Vector3(SelectedCurrent.transform.position.x, SelectedCurrent.transform.position.y + SelectedCurrent.transform.localScale.y + 2f, SelectedCurrent.transform.position.z);
                    SeletctObject.transform.localScale = SelectedCurrent.transform.localScale;
                    SeletctObject.transform.position = new Vector3(SelectedCurrent.GetComponentInChildren<Animator>().gameObject.transform.position.x, SelectedCurrent.GetComponentInChildren<Animator>().gameObject.transform.position.y + 1.2f, SelectedCurrent.GetComponentInChildren<Animator>().gameObject.transform.position.z);

                }
                  else
                {

                    if (SelectedCurrent.CompareTag("Resource"))
                    {

                        CArrow.transform.position = new Vector3(SelectedCurrent.transform.position.x, SelectedCurrent.transform.position.y + SelectedCurrent.transform.lossyScale.y + 2f, SelectedCurrent.transform.position.z);
                        SeletctObject.transform.localScale = SelectedCurrent.transform.localScale;
                        SeletctObject.transform.position = new Vector3(SelectedCurrent.transform.position.x, SelectedCurrent.gameObject.transform.position.y, SelectedCurrent.gameObject.transform.position.z);

                        if (SelectedCurrent.GetComponent<Resource>().resourceTyp == Resource.ResourceTyp.Axe)
                        {
                            SeletctObject.GetComponent<MeshFilter>().sharedMesh = SelectMesh;
                            Debug.Log("AXE");
                        }
                        else
                        {
                            SeletctObject.GetComponent<MeshFilter>().sharedMesh = SelectedCurrent.GetComponent<MeshFilter>().sharedMesh;
                        }
                    }

                }
            }
        }
        else
        {

            if (SelectedCurrent != null)
            {

                if (SelectedCurrent.GetComponent<MeshRenderer>())
                {
                    if (DC.GetComponent<DroneController>().currentMoveState != MoveState.ScaningObjcets)

                    {
                        if (SelectedCurrent.GetComponent<MeshRenderer>().material.HasProperty("_Boolean_Wind"))
                        {

                            SelectedCurrent.GetComponent<MeshRenderer>().material.SetInt("_Boolean_Wind", 1);
                        }
                    }

                }

                Destroy(SeletctObject.gameObject);
                Destroy(CArrow.gameObject);

            }
        }
    }

     
   
}