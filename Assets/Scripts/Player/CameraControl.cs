﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    public Transform target;
    public Transform RotationAncor;
    public float rotationSpeed;
    public bool useOffsetValues;
    public bool Camerarotation;
    public Vector3 offset;

    public GameObject Player;

    public float YPos ;
    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;


    public Transform pivot;
    // Use this for initialization
    void Start () {

        

        gameObject.transform.position = new Vector3(transform.position.x,YPos,transform.position.z);


        if (!useOffsetValues)
        {
            offset = target.position - transform.position;
        }

        pivot.transform.SetParent(target.transform);
    }
	
	// Update is called once per frame
	void FixedUpdate () {

 
            transform.LookAt(target);


        float dist = Vector3.Distance(transform.position,target.transform.position);

        if (dist > 15)
        {


         // transform.position = Vector3.MoveTowards(new Vector3(transform.position.x, YPos, transform.position.z), new Vector3(target.position.x, YPos, target.position.z), 0.05f);
           
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {
 
                Quaternion camTurnAngle = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * rotationSpeed, Vector3.up);

                offset = camTurnAngle * offset;

        }

       

        Vector3 newPos = target.position + offset;

        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);



    }
}
