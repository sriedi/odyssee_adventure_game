﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerNavMesh : MonoBehaviour
{
    // Camera  //--------------------------------------------------------------------

    [Header("Camera")]
    public Camera cam;

    // Pickup  //--------------------------------------------------------------------
    public Pickup pickup;


    // Player  //--------------------------------------------------------------------
    GameObject PlayerMesh;
    RaycastHit hit;

    // Cursor  //--------------------------------------------------------------------

    public GameObject DefatCurser;
    public GameObject OverWater;
    public GameObject cursor;

    public Texture2D CursorSprite;
    public Vector2 hotspot = Vector2.zero;
    



    // Target  //--------------------------------------------------------------------

    [Header("Target")]
    public Material Pmat;
    public Mesh PlaneMash;

    public GameObject Target;

    //[HideInInspector]
    public GameObject TargetCurrent;
    [HideInInspector]
    public GameObject TG;


    // Dialog  //--------------------------------------------------------------------

    [Header("Dialog")]
    public bool DialogIsActive;

    //  Movment  //------------------------------------------------------------------

    [Header("Movment")]
    [HideInInspector]
    public bool moveToObject;

    [HideInInspector]
    public bool Movement = true;

    public NavMeshAgent navMeshAgent;

    [HideInInspector]
    public NPCDialoge NPCD;

    Object_Interaction OI;

    [HideInInspector]
    public bool InteractRangeActive;

    Vector3 Tpos;
    float dist;
    Vector3 HO;

    //  Animation  //---------------------------------------------------------------------------------------

    public Animator animator;
    public AnimationClip AnimClip;

    public bool Pointonit;
    float blend;


   //  Hit Something  //------------------------------------------------------------------------------------


    [HideInInspector]
    public GameObject HitObjcet;

    [HideInInspector]
    public Vector3 HitPos;

    [HideInInspector]
    public Vector3 HitPosCurrent;


    public string CT;

    // Paused //-------------------------------------------------------------------------------------------

    public bool Paused;

    // Interact Range //-----------------------------------------------------------------------------------
    [Header("Gizmo")]
    [Range(min: 1, max: 10)]
    public float InteractR = 5;

    // Gizmo //--------------------------------------------------------------------------------------------

    public bool GizmosActive = true;

    // Selection //----------------------------------------------------------------------------------------

   public GameObject SelectPreview;

    void Start()
    {
        Cursor.SetCursor(CursorSprite, hotspot, CursorMode.ForceSoftware);
      

        pickup = GetComponent<Pickup>();

        PlayerMesh = gameObject.transform.GetChild(2).gameObject;

        cam = Camera.main;
        navMeshAgent = GetComponent<NavMeshAgent>();
        Movement = true;

        animator = gameObject.transform.GetChild(2).GetComponent<Animator>();

    }

    private void Update()
    {
        CurserAction();
        if (GetComponent<Pickup>().Carry)
        {
            navMeshAgent.speed = 5f;
        }
        else
        {
            navMeshAgent.speed = 10f;
        }


        PlayerMesh.transform.position = new Vector3(navMeshAgent.transform.localPosition.x, navMeshAgent.transform.localPosition.y, navMeshAgent.transform.localPosition.z);
        PlayerMesh.transform.localRotation = new Quaternion(0,0,0,0);
        
        PointOnIt();
        PlayerPaused();
        Animaionen();

        if (!Paused)
        {
            Cursor.visible = false;
            if (hit.collider != null) {
                dist = Vector3.Distance(transform.position, hit.collider.transform.position);
            }


            if (Movement)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    //Debug.Log("click _A");
                    Tagets();

                    if (!GameObject.FindGameObjectWithTag("Target") || Target == null)
                    {
                        if (CT != "Water" && CT != "NPC")
                        {
                            TargetCurrent = Instantiate(Target, Tpos, Quaternion.identity);
                        }
                    }

                    else
                    {
                        if (Movement)
                        {
                            TargetCurrent = GameObject.FindGameObjectWithTag("Target");
                            TargetCurrent.transform.position = Tpos;
                        }
                    }

                }

            }
        }
        else
        {
            Cursor.visible = true;
        }

    TargetLiveTime();

    }

// Update is called once per frame
    void FixedUpdate()
    {

    if (!Paused)
    {

        OI = gameObject.GetComponent<Object_Interaction>();

        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(cam.transform.position, ray.direction);


        if (Physics.Raycast(ray, out hit))
        {
            CT = hit.collider.tag;
            HitPos = hit.point;
            // Debug.Log("Object: " + CT);




            if (!hit.collider.CompareTag("Ground") && hit.collider.gameObject.GetComponent<Interactable>())
            {
                HitObjcet = hit.collider.gameObject;

            }
            else
            {
                HitObjcet = null;
            }

        }

    }
}
    

 

    void TargetLiveTime()
    {
        if (TargetCurrent != null)
        {

            Debug.DrawLine(TargetCurrent.transform.position, gameObject.transform.position,Color.red);



            float dist = Vector3.Distance(TargetCurrent.transform.position, gameObject.transform.position);

            //Debug.Log(dist);

           
                if (CT == "Ground")
                {

                    if (dist < 2)
                    {
                        Debug.Log("inRange");
                        DestroyTarget();

                    }

                    if (Input.GetKeyDown(KeyCode.Mouse1))
                    {
                    // Debug.Log(NPCD);
                        if (NPCD != null) {
                        NPCD.GetComponentInChildren<SpeechBubble>().ClearSymbols();
                        }
                    }

                 

                }
                else if (CT == "Resource")
                {

                    if (dist < 2)
                    {
                        DestroyTarget();
                    }
                }

                else if (CT == "Resource_button")
                {
                    DestroyTarget();
                }

            

        }
    }



    void Tagets()
    {







        switch (CT)

        {
            case "Obsticle":
                MoveToObject();
                break;


            case "Resource":
               // Debug.Log("Object Tag: " + CT);
                MoveToObject();

               
                break;

            case "Ground":
                SetTarget();
                MoveToPoint();
                break;

            case "Water":
                

                break;

            case "Resource_button":
                // MoveToObjct();
                break;

            case "RocketPart_1":
                MoveToObject();
                break;

            case "RocketPart_2":
                MoveToObject();
                break;

            case "RocketPart_3":
                MoveToObject();
                break;

            case "NPC":
              //  MoveToObject();
                break;

        }

    }

    public void MoveToObject()
    {
        if (Movement)
        {
            HO = HitObjcet.transform.position;
            navMeshAgent.SetDestination(HO);
            Tpos = HO;
            navMeshAgent.stoppingDistance = 1.8f;
        }
    }

    public void MoveToPoint()
    {
        if (Movement)
        {
            navMeshAgent.SetDestination(HitPos);
            Tpos = HitPos;
            navMeshAgent.stoppingDistance = 0f;
        }

    }


    public void SetTarget()
    {
        if (Movement)
        {
            if (TargetCurrent != null)
            {
                TargetCurrent.GetComponent<MeshFilter>().mesh = PlaneMash;
                TargetCurrent.GetComponent<Renderer>().material = Pmat;
                TargetCurrent.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                TargetCurrent.transform.SetParent(null);
                TargetCurrent.transform.rotation = new Quaternion(0, 0, 0, 0);
            }
        }
    }


    public void DestroyTarget()
    {
        Destroy(TargetCurrent.gameObject);
      //  Debug.Log("kill Target");
    }







    public void InteractRange()
    {

        if (HO != null) {
            float dist = Vector3.Distance(gameObject.transform.position, HO);
            Debug.DrawLine(gameObject.transform.position, HO, Color.red);
            Debug.Log("IR");
            if (NPCD != null)
            {
                NPCD.DisplayCurrentPhase();

            }
        }

    }

    public void PlayerPaused()
    {
        if (Paused) {

            navMeshAgent.isStopped = true;
        }
        else
        {
            navMeshAgent.isStopped = false;
        }

    }



    private void OnDrawGizmos()
    {

        Gizmos.color = Color.blue;

        if (HitObjcet != null && HitObjcet.GetComponent<MeshFilter>()) {

           // Gizmos.DrawWireMesh(HitObjcet.GetComponent<MeshFilter>().mesh, HO, HitObjcet.transform.rotation, new Vector3(HitObjcet.transform.localScale.x + 0.3f, HitObjcet.transform.localScale.y + 0.3f, HitObjcet.transform.localScale.z + 0.3f));
        }
        if (GizmosActive)
        {

            Gizmos.DrawWireSphere(gameObject.transform.position, InteractR);
        }


    }


    public void Animaionen()
    {
    

        if (animator.GetBool("PointOnIt"))
        {
            navMeshAgent.updateRotation = false;
            Movement = false;

           // navMeshAgent.transform.LookAt(new Vector3(.transform.position.x, 0, navMeshAgent.transform.position.z));

           // Debug.Log(navMeshAgent.transform.rotation);


            Vector3 lookVecor = TG.transform.position - gameObject.transform.position;
            Vector3 lerpLookDir = Vector3.Lerp(lookVecor,transform.forward, 0.9f);
            transform.LookAt(transform.position + lerpLookDir);
   

        }

        if (navMeshAgent.velocity != new Vector3(0, 0, 0))
        {
            animator.SetTrigger("IsMoving");
            animator.ResetTrigger("Idle");
        }

        else
        {
            animator.SetTrigger("Idle");
            animator.ResetTrigger("IsMoving");
        }


        if (pickup.Carry)
        {
            animator.SetLayerWeight(1, 1f);
        }
        else
        {
            animator.SetLayerWeight(1, 0f);
        }

    }


   public void PointOnIt()
    {
        if (animator.GetBool("PointOnIt"))
        {  
            animator.Play("Pointing", 2);
           
        }
        else
        {    

            animator.Play("Donothing", 2);
            navMeshAgent.updateRotation = true;
            Movement = true;
        }
  

    }




    public void CurserAction()
    {

        if (!Cursor.visible && CT == "Ground")
        {
            cursor.SetActive(true);
            cursor.transform.position = HitPos;
            cursor.GetComponent<MeshFilter>().sharedMesh = DefatCurser.GetComponent<MeshFilter>().sharedMesh;
        }

        else
        {
            cursor.SetActive(false);


        }

        if (!Cursor.visible && CT == "Water")
        {
            cursor.SetActive(true);
            cursor.transform.position = HitPos;
            cursor.GetComponent<MeshFilter>().sharedMesh = OverWater.GetComponent<MeshFilter>().sharedMesh;

            cursor.transform.LookAt(new Vector3(Camera.main.transform.position.x, cursor.transform.position.y, Camera.main.transform.position.z));

        }
    }

}//Monobehaviour
