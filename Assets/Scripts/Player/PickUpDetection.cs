﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpDerection : MonoBehaviour
{

    // Detectet Object //---------------------------------------------------------
    [Header("Tag List")]
    public List<string> ObjectToDetect;

    // NPC Finder //---------------------------------------------------------
    [Header("In Viewrange Objects")]
    public List<GameObject> NPCInRange;
    // All Objects in the Scene //---------------------------------------------------------
    [HideInInspector]
    public List<GameObject> ExistingObjects;
    GameObject Current_NPC;
    //public bool ShowTime;
    public bool StartTimer;
    float CountDown;
    public float Timer = 5f;
    PlayerNavMesh PNM;
    List<Symbol> Symbols;
    // NPC Interact Range  //---------------------------------------------------------

    [Header("Point on it Range")]
    [Range(min: 1, max: 20)]
    public float PointOnItSlider = 10;

    [Range(min: 1, max: 20)]
    public float PickUpRange = 10;

    public bool GizmoIsActive = true;
    void Start()
    {
       // ShowTime = true;
        ExistingCheck();
        CountDown = Timer;
    }
    // Update is called once per frame
    void Update()
    {
        PNM = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerNavMesh>();
        //DisplayTimer();
        if (ExistingObjects.Count != 0)
        {
            foreach (GameObject go in ExistingObjects)
            {  
                    float dist = Vector3.Distance(gameObject.transform.position, go.transform.position);

                    if (dist < PointOnItSlider)
                    {
                        if (!NPCInRange.Contains(go))
                        {
                            NPCInRange.Add(go);
                        }
                    }
                    else
                    {
                        if (NPCInRange.Contains(go))
                        {
                            NPCInRange.Remove(go);
                        }
                    }
            }  
        }
    }


    void ExistingCheck()
    {
        foreach (string Tag in ObjectToDetect)
        {
            if (ObjectToDetect.Count != 0 && Tag != "")
            {
                ExistingObjects.AddRange(GameObject.FindGameObjectsWithTag(Tag));
            }
        }
    }

    public void ReactionCheck()
    {
        if (NPCInRange.Count != 0)
        {
                foreach (GameObject Go in NPCInRange)
                {

                    if (Go.CompareTag("NPC"))
                    {
                   
                        Go.transform.GetChild(1).GetComponent<SpeechBubble>().ClearSymbols();
                        Symbols = gameObject.GetComponent<PointOnItTextHandler>().pointOnIt();
                        Go.transform.GetChild(1).GetComponent<SpeechBubble>().InjectSymbols(Symbols);

                        Current_NPC = Go.transform.GetChild(1).gameObject;
                    }

                    if (Go.CompareTag("Resource"))
                    {

                        if (PNM.HitObjcet == Go)
                        {
                            GetComponent<Pickup>().PickUpObject = Go;
                        }
                    }

                }
  
        }
    }



    private void OnDrawGizmos()
    {
        if (GizmoIsActive) {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(gameObject.transform.position, PointOnItSlider);
        }
    }
}


    
