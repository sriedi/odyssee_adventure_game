﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Failsave : MonoBehaviour
{

    Vector3 savepos; 

    // Start is called before the first frame update
    void Start()
    {
        savepos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y+5f, gameObject.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.y <= 3)
        {
            gameObject.transform.position = savepos;
        }
    }
}
