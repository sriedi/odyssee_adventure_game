﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    GameObject DO;
    Object_Interaction IO;
   public GameObject Player;

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward);
        Player = GameObject.FindGameObjectWithTag("Player");
        IO = Player.GetComponent<Object_Interaction>();



        if (Player.GetComponent<PlayerNavMesh>().HitObjcet != null)
        {
            DO = IO.SelectedObject;
        }

        ButtonOrientation();

    }

    void ButtonOrientation()
    {
        if (DO != null)
        {
            transform.position = new Vector3(DO.transform.position.x, DO.transform.position.y + DO.transform.localScale.y + 1, DO.transform.position.z);
            transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward);
        // transform.LookAt(Camera.main.transform, Vector3.up);

        }
    }
}
