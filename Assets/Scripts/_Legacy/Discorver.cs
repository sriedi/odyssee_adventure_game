﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Discorver : MonoBehaviour
{
public List<Symbol> SymbolToDiscover;
public List<Pattern> PatternToDiscover;

[HideInInspector]
public GameObject journalObj;
    // Start is called before the first frame update
    void Start()
    {
        if(!journalObj){
            journalObj = GameObject.Find("JournalUI");
        }
    }
    public void discover(){
        if(SymbolToDiscover != null){
            foreach (Symbol symbol in SymbolToDiscover)
            {
                journalObj.GetComponentInChildren<JournalContent>().AddSymbol(symbol);
            }
        }
        if(PatternToDiscover != null){
            foreach (Pattern pattern in PatternToDiscover){
                journalObj.GetComponentInChildren<PatternList>().AddPattern(pattern);
            }
            {
                
            }
        }
    }
}
