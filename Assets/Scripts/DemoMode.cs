﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DemoMode : MonoBehaviour
{
    public float cooldownTime = 300;
    public bool demoModeActive = false;
    bool timerActive = false;
    float timeStarted;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      if(demoModeActive){
      if(!Input.anyKey){
        if(!timerActive){
          Debug.Log("Demo Timer is now Active "+ Time.timeSinceLevelLoad);
          timerActive = true;
          timeStarted = Time.timeSinceLevelLoad;
        }

      }
      else
      {
        timerActive = false;
        Debug.Log("Demo Timer deactivated");
      }
      if(timerActive){
        
        float currentTime = Time.timeSinceLevelLoad;
        if(currentTime-timeStarted > cooldownTime){
          SceneManager.LoadScene("MainMenu");
          Debug.Log("Loading Main Menu");
        }
      }
      }

    }
}
