﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EndOfTheGame : MonoBehaviour
{

    public  Quest MainQuest;
    public float DelayTime;

    public bool StartEnd;


    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
      

        if (MainQuest.QuestFinished)
        {

            DelayTime = Time.deltaTime;


            if (SceneManager.GetActiveScene().name != "Outtro" && StartEnd)
            {
                SceneManager.LoadScene("Outtro");
            }
        }

       
    }
}
