﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
  
public class GameLogic : MonoBehaviour
{
    // Don't Destroy //----------------------------------------------------------------------
    public GameObject dialogeUiObj;
    public GameObject journalUiObj;
    public GameObject gameLogicObj;
    public GameObject pauseMenuUiObj;
    public GameObject messageUiObj;
    public GameObject[] DontDestroyList;
    List<GameObject> DDL;

    public GameObject Player;
    // Game States //------------------------------------------------------------------------

     public  enum GameState
    {
          MainMenu,
          Level,
          Earlygame,
          MiddGame,
          LateGame,
          Intro,
          End
    }
    public GameState gameState;
    public enum InterfaceState{
        MainMenu,
        InGame,
        PauseMenu,
        Dialoge,
        Journal,
        Message
    }   
public InterfaceState interfaceState;
    public enum GameMode
    {
        Regular,
        TutorialMovment,
        TutorialInteract,
        TutorialPointOnIt,
        TutorialDialog,
        Paused
    }
    public GameMode gameMode;

    // Paused  //---------------------------------------------------------------------------

    public bool PausedMenuActivation;
    public bool PlayerMovement;
                                                                                       
    // Functions   //---------------------------------------------------------------------------
    public void Start()
    {

        Player = GameObject.FindGameObjectWithTag("Player");

        gameLogicObj = gameObject;
        if (pauseMenuUiObj == null){
            pauseMenuUiObj = GameObject.Find("PauseMenuUI").gameObject;
        }
        if(journalUiObj == null){
            journalUiObj = GameObject.Find("JournalUI").gameObject;
        }
        if(dialogeUiObj == null){
            dialogeUiObj = GameObject.Find("DialogeUI").gameObject;
        }

        if (pauseMenuUiObj == null){
            Instantiate(pauseMenuUiObj);
        }
        if (messageUiObj == null){
            messageUiObj = GameObject.Find("MessageUI").gameObject;
        }
        CloseUI();
    }

     


    private void Update()
    {
        checkStates();
    }
// Check the States / -------------------------------------------------------------------------------------
    public void checkStates(){
    switch (gameState)
         {
          // Menu //-----------------------------------------------------------------------------------
            case GameState.MainMenu:
                pauseMenuUiObj.GetComponent<Canvas>().enabled = false;
                if (SceneManager.GetActiveScene().name != "MainMenu")
                {     
                    SceneManager.LoadScene("MainMenu");
                }
                PausedMenuActivation = false;
                break;

          // Level //-----------------------------------------------------------------------------------
            case GameState.Level:
               if (SceneManager.GetActiveScene().name != "World")
                {
                    SceneManager.LoadScene("World");
                }
                
                break;

            case GameState.Intro:
                if (SceneManager.GetActiveScene().name != "Intro")
                {
                    SceneManager.LoadScene("Intro");
                }

                break;

            case GameState.End:
                if (SceneManager.GetActiveScene().name != "Outtro")
                {
                    SceneManager.LoadScene("Outtro");
                }

                break;

            // Progress //-----------------------------------------------------------------------------------  
            case GameState.Earlygame:
                break;
            case GameState.MiddGame:
                break;
            case GameState.LateGame:
                break;
        }

        switch (gameMode) {
            case GameMode.Regular:
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    Debug.Log("Pause");
                    gameMode = GameMode.Paused;
                    PausedMenuActivation = true;
                }
                 
                break;

            // Pause //-----------------------------------------------------------------------------------
            case GameMode.Paused:
                Debug.Log("GameMode is Paused");
                if (PausedMenuActivation){
                    interfaceState = InterfaceState.PauseMenu;
                }
                if (GameObject.FindGameObjectWithTag("Player"))
                {
                    Player.GetComponent<PlayerNavMesh>().Movement = false;
                    Player.GetComponent<PlayerNavMesh>().Paused = true; 
                }
                /* if (Input.GetKeyDown(KeyCode.Escape))
                {
                    CloseUI();
                    PausedMenuActivation = false;
                } */
                break;

            // Tutorial //-----------------------------------------------------------------------------------
            case GameMode.TutorialInteract:
                break;
            case GameMode.TutorialMovment:
                break;
            case GameMode.TutorialPointOnIt:
                break;
            case GameMode.TutorialDialog:
                break;
        }
        switch(interfaceState){
            case InterfaceState.InGame:
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    Debug.Log("Load Journal");
                    LoadJournal();
                }
                break;
            case InterfaceState.Dialoge:
                gameMode = GameMode.Paused;
                if(!dialogeUiObj.GetComponent<Canvas>().enabled){
                    journalUiObj.GetComponent<Canvas>().enabled = false;
                    messageUiObj.GetComponent<Canvas>().enabled = false;
                    pauseMenuUiObj.GetComponent<Canvas>().enabled = false;
                    dialogeUiObj.GetComponent<Canvas>().enabled = true;
                }
                Debug.Log(dialogeUiObj.GetComponent<Canvas>() + " Dialoge UI loaded");
                if(Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1)){
                    CloseUI();
                 }
                break;
            case InterfaceState.Journal:
                if(!journalUiObj.GetComponent<Canvas>().enabled){
                    messageUiObj.GetComponent<Canvas>().enabled = false;
                    pauseMenuUiObj.GetComponent<Canvas>().enabled = false;
                    journalUiObj.GetComponent<Canvas>().enabled = true;
                    Debug.Log("Journal loaded");
                    dialogeUiObj.GetComponent<Canvas>().enabled = false;
                }
                if(Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1)){
                    CloseUI();
                 }
                
                break;
            case InterfaceState.Message:
                    messageUiObj.GetComponent<Canvas>().enabled = true;
                    pauseMenuUiObj.GetComponent<Canvas>().enabled = false;
                    journalUiObj.GetComponent<Canvas>().enabled = false;
                    dialogeUiObj.GetComponent<Canvas>().enabled = false;
                break;
            case InterfaceState.PauseMenu:
                Debug.Log("Interface is PauseMenu");
                pauseMenuUiObj.GetComponent<Canvas>().enabled = true;
                gameMode = GameMode.Paused;
                break;
            case InterfaceState.MainMenu:
                break;
        }
    }


    // Interface //-----------------------------------------------------------------------------------------
    public void LoadDialogeMenu(){
        Debug.Log("Inputfield Loaded");
        interfaceState = InterfaceState.Dialoge;
        gameMode = GameMode.Paused;
    }
    public void LoadJournal(){
        interfaceState = InterfaceState.Journal;
        gameMode = GameMode.Paused;
    }
    public void showMessage(){
        interfaceState = InterfaceState.Message;
        gameMode = GameMode.Paused;
    }
    public void CloseUI(){
      //  Debug.Log("UI closed");
        interfaceState = InterfaceState.InGame;
        messageUiObj.GetComponent<Canvas>().enabled = false;
        dialogeUiObj.GetComponent<Canvas>().enabled = false;
        journalUiObj.GetComponent<Canvas>().enabled = false;
        pauseMenuUiObj.GetComponent<Canvas>().enabled = false;
        EndPause();
    }

    // Menu //----------------------------------------------------------------------------------------------
    public void LoadMainMenu()
    {
       gameState = GameState.MainMenu;
    }

    public void LoadLevel()
    {

        gameState = GameState.Level;
    }

    public void LoadIntro()
    {
        gameState = GameState.Intro;
    }

    public void LoadEnd()
    {
        gameState = GameState.End;
    }

    // Tutorials //-----------------------------------------------------------------------------------
    public void TutorialInteractActive()
    {
        gameMode = GameMode.TutorialInteract;
    }

    public void TutorialMovmentActive()
    {
        gameMode = GameMode.TutorialMovment;
    }

    public void TutorialPointOnItActive()
    {
        gameMode = GameMode.TutorialPointOnIt;
    }

    public void TutorialDialogActive()
    {
        gameMode = GameMode.TutorialDialog;
    }

  // Progress //-----------------------------------------------------------------------------------
    public void EarlygameActive()
    {
        gameState = GameState.Earlygame;
    }

    public void MiddGameActive()
    {
        gameState = GameState.MiddGame;
    }

    public void LateGameActive()
    {
        gameState = GameState.LateGame;
    }

    // Functoinen //-----------------------------------------------------------------------------------
    public void QuitGame()
    {
        Application.Quit(); 
    }

    public void EndPause()
    {
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            PlayerNavMesh Player = FindObjectOfType<PlayerNavMesh>();
            Player.Movement = true;
            Player.Paused = false;
        }
        //Debug.Log("Game Unpaused");
        pauseMenuUiObj.GetComponent<Canvas>().enabled = false;
        gameMode = GameMode.Regular;
        interfaceState = InterfaceState.InGame;
        PausedMenuActivation = false;

    }


}


