﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
public class NPCAudioHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public StudioEventEmitter footstepsEmitter;
    public StudioEventEmitter talkEmitter;
    public StudioEventEmitter prayEmitter;
    
    public StudioEventEmitter disagreeEmitter;
    public StudioEventEmitter agreeEmitter;
    public StudioEventEmitter panicEmitter;

    public void Step(){
        //Debug.Log("Step Sound Called");
        if(footstepsEmitter != null){
            footstepsEmitter.Play();
        }
    }
    public void Talk(){
        if(talkEmitter != null){
            talkEmitter.Play();
        }
    }

    public void Pray(){
        if(prayEmitter != null){
            Debug.Log(gameObject.name +" playing Pray Sound");
            prayEmitter.Play();
        }
    }
    public void Disagree(){
        if(disagreeEmitter != null){
            disagreeEmitter.Play();
        }
    }
    public void Agree(){
        if(agreeEmitter != null){
            agreeEmitter.Play();
        }
    }
    public void Panic(){
        if(panicEmitter != null){
            panicEmitter.Play();
        }
    }

}
