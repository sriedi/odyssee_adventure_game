﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
public class AudioHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public StudioEventEmitter footstepsEmitter;
    public StudioEventEmitter pointOnItEmitter;
    public StudioEventEmitter pickUpEmitter;
    public StudioEventEmitter dropEmitter;


    public void Step(){
        //Debug.Log("Step Sound Called");
        if(footstepsEmitter != null){
            footstepsEmitter.Play();
        }
    }
    public void Point(){
        if(pointOnItEmitter != null){
            pointOnItEmitter.Play();
        }
    }
    public void PickUp(){
        if(pickUpEmitter != null){
            pickUpEmitter.Play();
        }
    }
    public void DropIt(){
        if(dropEmitter != null){
            dropEmitter.Play();
        }
    }
}
