﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scan_Beam : MonoBehaviour
{

    public GameObject StartPos;
    [HideInInspector]
    public GameObject EndPos;


    public GameObject Selection;
    GameObject ScanObject;

    PlayerNavMesh PNM;

    DroneController DC;



    // Start is called before the first frame update
    void Start()
    {
        PNM = FindObjectOfType<PlayerNavMesh>();
        DC = FindObjectOfType<DroneController>();

        EndPos = StartPos;
    }

    // Update is called once per frame
    void Update()
    {
        Direction();

       // StartPos.transform.position = GetComponent<Collider>().transform.position;
        transform.position = StartPos.transform.position;
        transform.SetParent(StartPos.transform);

        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Object_Interaction>().SelectedCurrent)
        {
            Selection = GameObject.FindGameObjectWithTag("Player").GetComponent<Object_Interaction>().SeletctObject;
        }

    }

    void Direction()
    {
       
        if (EndPos != null)
        {
            if (DC.currentMoveState == MoveState.ScaningObjcets)
            {
                if (EndPos.GetComponent<MeshRenderer>())
                {
                    if (EndPos.GetComponent<MeshRenderer>().material.HasProperty("_Boolean_Wind"))
                    {

                        EndPos.GetComponent<MeshRenderer>().material.SetInt("_Boolean_Wind", 0);
                    }
                }

                if (ScanObject == null)
                {
                 

                    if (EndPos.CompareTag("Drone"))
                    {
                        ScanObject = Instantiate(Selection, new Vector3(EndPos.transform.position.x, EndPos.transform.position.y+0.3f, EndPos.transform.position.z), EndPos.transform.rotation);
                    }

                    else if (EndPos.CompareTag("NPC"))
                    {
                        ScanObject = Instantiate(Selection, new Vector3(EndPos.transform.position.x, EndPos.transform.position.y + 1.2f, EndPos.transform.position.z), EndPos.transform.rotation);
                    }
                    else 
                    {
                        ScanObject = Instantiate(Selection, new Vector3(EndPos.transform.position.x, EndPos.transform.position.y, EndPos.transform.position.z), EndPos.transform.rotation);
                    }

                   
                }
                GetComponent<MeshRenderer>().enabled = true;

                Vector3 direction = StartPos.transform.position - EndPos.transform.position;

                //EndPos.GetComponent<MeshRenderer>().material.SetInt("_Boolean_Wind", 0);

                gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, direction.magnitude, gameObject.transform.localScale.z);
                gameObject.transform.up = direction;
            }
        else 
            {
                if (EndPos.GetComponent<MeshRenderer>())
                {
                    if (EndPos.GetComponent<MeshRenderer>().material.HasProperty("_Boolean_Wind"))
                    {

                        EndPos.GetComponent<MeshRenderer>().material.SetInt("_Boolean_Wind", 1);
                    }
                }

                GetComponent<MeshRenderer>().enabled = false;
                //EndPos.GetComponent<MeshRenderer>().material.SetInt("_Boolean_Wind", 1);
                Destroy(ScanObject);
                EndPos = null;
            }
        }
        else
        {
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                EndPos = PNM.HitObjcet;
            }
            
        }
    }
}
