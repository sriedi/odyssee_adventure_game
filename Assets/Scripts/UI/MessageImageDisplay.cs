﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using TMPro;

public class MessageImageDisplay : MonoBehaviour
{
    Sprite imageSprite;
    Image messageImage;
    
    // Start is called before the first frame update
    void Awake()
    {
        messageImage = gameObject.GetComponent<Image>();
        messageImage.enabled = false;   
    }

    // Update is called once per frame
    void Update()
    {
        if(imageSprite != null){
            messageImage.enabled = true;
            messageImage.sprite = imageSprite;
        }
        
    }
    public void SetSprite(Sprite inSprite){
        messageImage.enabled = true;
        imageSprite = inSprite;
    }
    public void UnsetImage(){
        messageImage.enabled = false;
        imageSprite = null;
    }
}
