﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public bool hasDialoge = false;
    public bool hasDetection = false;
    NPCDialoge objectDialogeComp;
    Detection objectDetectionComp;
    public bool hasScanData = false;
    PlayerNavMesh playerNavMesh;
    DroneController droneController;
    Pickup pickUpComp;
    // Start is called before the first frame update
    private void Start() {
        pickUpComp = FindObjectOfType<Pickup>(); 
        playerNavMesh = FindObjectOfType<PlayerNavMesh>();
        droneController = FindObjectOfType<DroneController>();
    }
//Check the Components. This gets executed once a raycast hit.
    public void CheckComponents(){

        if(gameObject.GetComponent<NPCDialoge>() != null){
        hasDialoge = true;
        objectDialogeComp = gameObject.GetComponent<NPCDialoge>();
        }
        if(gameObject.GetComponent<Detection>() != null){
            hasDetection = true;
            objectDetectionComp = gameObject.GetComponent<Detection>();
        }
        if(gameObject.GetComponent<ScanData>() != null){
            hasScanData = true;
        }
    }
// Methods to be used by the Buttons
    public void StartDialoge(){
        objectDialogeComp.DisplayCurrentPhase();
    }
    public bool IsPickUp(){
        if(pickUpComp.PickUpObject != gameObject){
            return false;
        }
        if(!pickUpComp.inRange){
            return false;
        }
        return true;
    }
    public bool HasPickUp(){
        if(gameObject.GetComponent<Resource>()){
            return true;
        }
        return false;
    }

    public bool CanDrop(){
        if(gameObject.GetComponent<Pickup>()!= null){
            if(pickUpComp.Carry){
                return true;
            }
        }
        return false;
    }
    public void PickItUp(){
        pickUpComp.PickUp();
    }
    public void PointOnIt(){
        playerNavMesh.TG = gameObject;
        objectDetectionComp.ReactionCheck();
        playerNavMesh.animator.SetTrigger("PointOnIt");
    }
    public void trigggerScan(){
        droneController.orderPosition(gameObject);
    }
    public void dropItem(){
        pickUpComp.Drop();
    }
}
