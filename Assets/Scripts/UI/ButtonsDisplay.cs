﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsDisplay : MonoBehaviour
{
    bool displayActive = false;
    Vector3 viewportPoint;
    GameObject followObject;
    public GameObject buttonTalk;
    public GameObject buttonPoint;
    public GameObject buttonScan;
    public GameObject buttonCarry;
    public GameObject buttonDrop;
    public GameObject buttonCarryDisabled;
    public  Interactable currentInteractable;
    GameLogic gameLogic;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Canvas>().enabled = false;
        buttonTalk.SetActive(false);
        buttonPoint.SetActive(false);
        buttonScan.SetActive(false);
        buttonCarry.SetActive(false);
        buttonCarryDisabled.SetActive(false);
        gameLogic = FindObjectOfType<GameLogic>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!displayActive){
            if(Input.GetMouseButtonUp(1)){
            CheckObjects();
            Debug.Log("Button Triggered");
            }
        }
        if(displayActive){
            PositionButtons();
            if(Input.GetMouseButtonUp(0)){
                EndDisplay();
            }
        }
    }
    public void CheckObjects(){
        Ray rayCheckObjects = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(rayCheckObjects,out hit)){
            if(hit.transform.gameObject.GetComponent<Interactable>()!= null){
                followObject = hit.transform.gameObject;
                currentInteractable = hit.transform.gameObject.GetComponent<Interactable>();
                currentInteractable.CheckComponents();
                EvaluateButtons();
                DisplayButtons();
            }
        }
    }
    
    public void DisplayButtons(){
        displayActive = true;
        gameObject.GetComponent<Canvas>().enabled = true;
        PositionButtons();
        gameLogic.gameMode = GameLogic.GameMode.Paused;
    }
    void EvaluateButtons(){
        if(currentInteractable.hasDialoge){
            buttonTalk.SetActive(true);
        }
        else{buttonTalk.SetActive(false);}
        if(currentInteractable.hasDetection){
            buttonPoint.SetActive(true);
        }
        else{buttonPoint.SetActive(false);}
        if(currentInteractable.hasScanData){
            buttonScan.SetActive(true);
        }
        else{buttonScan.SetActive(false);}
        if(currentInteractable.HasPickUp()){
            if(currentInteractable.IsPickUp()){
                buttonCarry.SetActive(true);
                buttonCarryDisabled.SetActive(false);
            }
            else{
                buttonCarryDisabled.SetActive(true);
            }
        }
        else{
            buttonCarryDisabled.SetActive(false);
            buttonCarry.SetActive(false);
        }
        if(currentInteractable.CanDrop()){
             buttonDrop.SetActive(true);
        }
        else{buttonDrop.SetActive(false);}
    }
    void PositionButtons(){
        viewportPoint = Camera.main.WorldToScreenPoint(followObject.transform.transform.position);
        viewportPoint.z = 0; 
        gameObject.GetComponent<RectTransform>().position = viewportPoint;
    }
    
    public void EndDisplay()
    {
        displayActive = false;
        gameObject.GetComponent<Canvas>().enabled = false;
        gameLogic.EndPause();
    }
    public void ExecuteScan(){
        EndDisplay();
        currentInteractable.trigggerScan();
    }
    public void ExecuteDialoge(){
        EndDisplay();
        currentInteractable.StartDialoge();
    }
    public void ExceutePointOnIt(){
        EndDisplay();
        currentInteractable.PointOnIt();
    }

    public void ExecutePickUp(){
        EndDisplay();
        currentInteractable.PickItUp();
    }
    public void ExecuteDrop(){
        EndDisplay();
        currentInteractable.dropItem();
    }

}
