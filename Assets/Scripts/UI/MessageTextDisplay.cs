﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MessageTextDisplay : MonoBehaviour
{
    TextMeshProUGUI messageText;
    string messageString;
    // Start is called before the first frame update
    void Awake()
    {
        messageText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if(messageString != null){
            messageText.text = messageString;
        }
    }

    public void SetString(string inString){
        messageString = inString;
    }
    public void UnsetString(){
        messageString = null;
    }
}
