﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using FMODUnity;

using TMPro;
public class MessageUI : MonoBehaviour
{
    MessageTextDisplay textDisplay;
    MessageImageDisplay imageDisplay;
    GameLogic gameLogic;
    public UnityEvent onMessageClose;
    public StudioEventEmitter messageUISoundEmitter;

    

    // Start is called before the first frame update
    void Start()
    {
        gameLogic = FindObjectOfType<GameLogic>();
        if(!messageUISoundEmitter){
            messageUISoundEmitter = gameObject.GetComponent<StudioEventEmitter>();
        }
    }

    void Awake() {
        textDisplay = gameObject.GetComponentInChildren<MessageTextDisplay>();    
        imageDisplay = gameObject.GetComponentInChildren<MessageImageDisplay>();
    }

    //pdate is called once per frame
    void Update()
    {
        
    }
    public void DisplayMessage(string inString){
        textDisplay.SetString(inString);
        imageDisplay.UnsetImage();
        gameLogic.showMessage();
        PlayAudio();
    }
    public void DisplayMessage(string inString, Sprite inSprite){
        imageDisplay.SetSprite(inSprite);
        textDisplay.SetString(inString);
        gameLogic.showMessage();
        PlayAudio();
    }
    public void InjectMessage(string inString, Sprite inSprite, UnityEvent inEvent){
        if(inSprite != null){
            imageDisplay.SetSprite(inSprite);        
        }
        if(inEvent != null){
            onMessageClose = inEvent;
        }
        textDisplay.SetString(inString);
        gameLogic.showMessage();
        PlayAudio();
    }
    public void CloseMessage(){
        imageDisplay.UnsetImage();
        textDisplay.UnsetString();
        gameLogic.CloseUI();
        if(onMessageClose != null){
            onMessageClose.Invoke();
        }
    }
    public void PlayAudio(){
        if (messageUISoundEmitter !=null)
        {
            messageUISoundEmitter.Play();
        }
    }

}
