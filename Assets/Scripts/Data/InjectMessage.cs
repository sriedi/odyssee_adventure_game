﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InjectMessage : MonoBehaviour
{
    public Sprite messageSprite;
    [TextArea]
    public string messageDescription;
    MessageUI messageSystem;

    [SerializeField]
    UnityEvent onTriggerEvent;
    public bool MessageTriggered = false;

    // Start is called before the first frame update
    void Start()
    {
        if(!messageSystem){
          messageSystem = FindObjectOfType<MessageUI>();
      }   
    }

    public void InjectMessageUI(){
        if(!MessageTriggered){
            Debug.Log("Message Injected");
            messageSystem.InjectMessage(messageDescription,messageSprite,onTriggerEvent);
            MessageTriggered = true;
        }
    }
    public void InjectMessageWithDelay(float inTime){
        Invoke("InjectMessageUI",inTime);
    }
}
