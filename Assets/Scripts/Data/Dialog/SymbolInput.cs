﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using FMODUnity;
public class SymbolInput : MonoBehaviour , IPointerClickHandler
{
    Symbol inputSymbol;
    InputData inputDataObj;
    public void setSymbolSprites(){
       gameObject.GetComponentInChildren<Image>().sprite = inputSymbol.symbolSprite;
    }

    public void setSymbol (Symbol inSymbol) {
        inputSymbol = inSymbol;
        setSymbolSprites();
    }
     public void OnPointerClick(PointerEventData eventData){
        gameObject.GetComponent<StudioEventEmitter>().Play();
        inputDataObj = gameObject.transform.parent.GetComponent<InputData>();
        inputDataObj.inputSymbols.Remove(inputSymbol);
        Debug.Log("Clicked on "+inputSymbol);
        Destroy(gameObject);
     }
}
