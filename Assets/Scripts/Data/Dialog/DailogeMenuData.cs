﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailogeMenuData : MonoBehaviour
{
    public List<Symbol> dialogSymbols;
    public GameObject menuSymbolPrefab;
    // Start is called before the first frame update
    void Start()
    {
        createSymbols();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void createSymbols(){
    foreach (Symbol symbol in dialogSymbols)
        {
            GameObject instObj = Instantiate(menuSymbolPrefab,gameObject.transform,false);
            instObj.GetComponent<SymbolMenu>().setSymbol(symbol);
            instObj.GetComponent<SymbolMenu>().setSymbolSprites();
        }
    }
    public void addSymbol(Symbol inSymbol){
        if(detectDoubles(inSymbol)){
            dialogSymbols.Add(inSymbol);
            GameObject instObj = Instantiate(menuSymbolPrefab,gameObject.transform,false);
            instObj.GetComponent<SymbolMenu>().setSymbol(inSymbol);
            instObj.GetComponent<SymbolMenu>().setSymbolSprites();
        }
    }
    bool detectDoubles(Symbol inSymbol){
        foreach (Symbol symbol in dialogSymbols)
        {
            if(symbol.Equals(inSymbol)){
                return false;
            }
        }
        return true;
    }
}
