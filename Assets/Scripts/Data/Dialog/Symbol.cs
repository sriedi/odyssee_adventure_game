﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
[CreateAssetMenu(fileName = "Symbole", menuName = "ScriptableObjects/Symbol", order = 1)]
public class Symbol : ScriptableObject
{
    [Header("Symbol")]
    public Sprite symbolSprite;
    [Header("Word")]
    public string[] word;
    [Header("Pattern")]
    public Pattern pattern;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
