﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BubbleDisplay : MonoBehaviour
{
    bool displayActive = false;
    Vector3 viewportPoint;
    GameObject followObject;
    public Image bubbleArrow;
    public float yOffset = 4.5f;

    // Start is called before the first frame update
    private void Start() {
      if(!bubbleArrow){
          bubbleArrow = GameObject.Find("BubbleArrow").GetComponent<Image>();
      }
    }

    private void Update() {
        if(displayActive){
           PositionSymbols();
        }
        else{
           bubbleArrow.enabled = false;
        }
    }

    public void DisplaySymbols(Vector3 inVector,GameObject inObject){
        followObject = inObject;
        displayActive = true;
        bubbleArrow.enabled = true;
        PositionSymbols();
    }
    void PositionSymbols(){
        viewportPoint = Camera.main.WorldToScreenPoint(followObject.transform.transform.position);
        viewportPoint.z = 0;
        viewportPoint.y += yOffset;
        gameObject.GetComponent<RectTransform>().position = viewportPoint;
    }
    
    public void EndDisplay()
    {
        displayActive = false;
    }
    

}
