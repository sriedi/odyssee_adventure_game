﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using FMODUnity;
using UnityEngine.EventSystems;

public class SymbolMenu : MonoBehaviour, IPointerClickHandler
{
    public Symbol symbol;
    InputData inputField;
    public Image symbolImage;
    // Start is called before the first frame update
    public void setSymbol (Symbol inSymbol) {
        symbol = inSymbol;
        setSymbolSprites();
    }
    private void Start() {
        setSymbolSprites();
    }
    public void OnPointerClick(PointerEventData eventData){
        inputField = FindObjectOfType<InputData>();
        if(inputField.inputSymbols.Count+1 < inputField.maxInputObj){
            gameObject.GetComponent<StudioEventEmitter>().Play();
            Debug.Log(symbol+" Clicked");
            inputField.inputSymbols.Add(symbol);
        }
    }
    public void setSymbolSprites(){
        symbolImage.sprite = symbol.symbolSprite;
        gameObject.GetComponentInChildren<TextMeshProUGUI>().text = symbol.word[0];
    }
}
