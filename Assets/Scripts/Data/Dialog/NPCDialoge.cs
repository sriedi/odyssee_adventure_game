﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class NPCDialoge : MonoBehaviour
{
    [SerializeField]
    public Dialoge npcDialogeObj;
    public bool inputState =false;

    //[HideInInspector]
    public InputData dialogeInputdata;
    [HideInInspector]
    public GameObject bubbleObj;
   // [HideInInspector]
    public GameLogic gameLogicObject;
    bool isInvokeing = false;
    //Used if we invoke stuff on runtime;


    // Start is called before the first frame update
    void Start()
    {
        if(!gameLogicObject){
            gameLogicObject = FindObjectOfType<GameLogic>();
        }
        dialogeInputdata = FindObjectOfType<InputData>();
        bubbleObj = gameObject.GetComponentInChildren<SpeechBubble>().gameObject;
        
    }


    // Update is called once per frame
    void Update()
    {   
        if(inputState){
            if(ValidateInput())
            {
                SolvePhase();
            }    
        }
    }
    //Displays current dialoge
    public void DisplayCurrentPhase(){
        if(!bubbleObj.activeInHierarchy){
            bubbleObj.SetActive(true);
        }
        gameObject.GetComponentInChildren<Animator>().SetTrigger("Talk");
        LangCheck currentLangCheck = npcDialogeObj.showCheck();
        Debug.Log("Show Dialoge of "+gameObject.name);
        gameObject.GetComponentInChildren<SpeechBubble>().SetSymbols(currentLangCheck.returnSymbols(),currentLangCheck.displayTime);
        gameObject.GetComponentInChildren<SpeechBubble>().DisplaySymbols();
        if(currentLangCheck.canAnswer){
            AllowInput();
        }
        else
        {
            DisallowInput();
        }
        
    }
    //Hide the Bubble
    public void HideCurrentPhase(){
        
        bubbleObj.SetActive(false);
        DisallowInput();
    }



    //Solve the Phase and go to the next Check
    public void SolvePhase(){
        isInvokeing = false;
        npcDialogeObj.showCheck().solveCheck();
        npcDialogeObj.currentPhase ++;
        inputState = false;
        gameObject.GetComponentInChildren<SpeechBubble>().ClearSymbols();
        dialogeInputdata.clearInput();
        DisplayCurrentPhase();
    }

    public void SolvePhaseTimed(float inFloat){
        if(!isInvokeing){
            isInvokeing = true;
            Invoke("SolvePhase",inFloat);
        }
    
    }
    //ANZEIGE & VALIDIERUNG
   
    //Display Input form if the player can input Words
    public void AllowInput(){
        gameLogicObject.LoadDialogeMenu();
        inputState = true;
    }
    // if there is nothing to say
    public void DisallowInput(){
        gameLogicObject.CloseUI();
        inputState = false;
        
    }
    //Validate the Player Word input !!Warning!! if you have an Input smaller than 2 it will not work.
    public bool ValidateInput(){
        List<Symbol> currentLangCheck = npcDialogeObj.showCheck().input;
        List<Symbol> currentInput = dialogeInputdata.inputSymbols;
       if(currentInput.Count<currentLangCheck.Count){
            return false;
        }
        for (int i = 0; i < currentLangCheck.Count-1; i++)
        {
            if(currentInput.Count == 0){
                return false;
            }
            if(i > currentInput.Count-1){
                return false;
            }
            if(!currentInput[i].Equals(currentLangCheck[i])){
                return false;
            }
        }
        
        return true;
    }
    
}
