﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialoge 
{
  [Header("LangChecks")]
  public List<LangCheck> phases;
  public int currentPhase = 0;
  public Dialoge(){

  }
  public LangCheck showCheck(){
    return phases[currentPhase];
  }
  
}
