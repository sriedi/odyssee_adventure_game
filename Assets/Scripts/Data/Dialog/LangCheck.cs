﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class LangCheck
{
    
    [SerializeField]
    public List<Symbol> content;
    [SerializeField]
    public List<Symbol> input;
    public bool canAnswer;
     [Range(2.0f, 15.0f)] 
    public float displayTime = 3.0f;

    bool isSolved = false;
    [Header("On display Event")]
    [SerializeField]
    public UnityEvent triggerOnDisplay;
    [Header("On solve Event")]
    [SerializeField]
    public UnityEvent triggerOnSolve;

    public LangCheck() {
    
    }

    public void solveCheck(){
        Debug.Log("Check Solved");
        isSolved = true;
        triggerOnSolve.Invoke();
    }
    public List<Symbol> returnSymbols()
    {   
        triggerOnDisplay.Invoke();
        return content;
    }

}

