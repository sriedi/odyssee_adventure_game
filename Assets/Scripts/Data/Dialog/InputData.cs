﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InputData : MonoBehaviour
{
    public List<Symbol> inputSymbols;
    public GameObject inputPrefab;
    public int childrenObjIndex;
    public float maxInputObj = 7;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        checkList();
    }

    void displaySymbol(int inIndex){
        GameObject instObj = Instantiate(inputPrefab,gameObject.transform,false);
        instObj.GetComponent<SymbolInput>().setSymbol(inputSymbols[inIndex]);
    }
    void checkList(){
        childrenObjIndex = gameObject.transform.childCount;
        if(childrenObjIndex<inputSymbols.Count){
            displaySymbol(childrenObjIndex);
        }
    }
   public void clearInput(){
        inputSymbols.Clear();
        foreach (Transform child in gameObject.transform)
        {
            Destroy(child.gameObject);
        }
    }

}
