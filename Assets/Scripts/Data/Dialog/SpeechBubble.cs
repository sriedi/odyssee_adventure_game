﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpeechBubble : MonoBehaviour
{
    public List<Symbol> bubbleSymbols;
    public GameObject symbolObjPrefab;
    BubbleDisplay bubbleDisplay;
    BubbleCanvas bubbleCanvas;
    SymbolDatabase symbolDatabase;
    //float xOffset = 1.2f;
    float displayDuration=5;//DefaultDuration is overwritten if setSymbols with float overload is called.
    float xPosition;
    // Start is called before the first frame update
    void Start()
    {
        if(!symbolDatabase){
            symbolDatabase = FindObjectOfType<SymbolDatabase>();
        }
        bubbleDisplay = FindObjectOfType<BubbleDisplay>();
        bubbleCanvas = FindObjectOfType<BubbleCanvas>();
    }

    // Update is called once per frame

    //Set Symbols 
    public void SetSymbols(List<Symbol> inSymbols){
        ClearSymbols();
        bubbleSymbols = new List<Symbol>(inSymbols);
    }
    //For a Custom Coroutine Duration
    public void SetSymbols(List<Symbol> inSymbols, float inTimer){
        SetSymbols(inSymbols);
        if(inTimer > 0){
        displayDuration = inTimer;
        }
    }
    public void DisplaySymbols(){
        bubbleDisplay.DisplaySymbols(gameObject.transform.position,gameObject);
       
        foreach (Symbol symbol in bubbleSymbols)
        {
            GameObject instObj = Instantiate(symbolObjPrefab,bubbleCanvas.gameObject.transform,false);
            instObj.GetComponentInChildren<SymbolSprite>().SetSymbol(symbol);
            if(symbolDatabase.solvedSymbols.Contains(symbol)){
                instObj.GetComponentInChildren<TextMeshProUGUI>().text = symbol.word[0];
            }
            else
            {
                instObj.GetComponentInChildren<TextMeshProUGUI>().text = " ";    
            }
            Invoke("DisplayTimer",displayDuration);
           
        }
        gameObject.GetComponentInParent<NPC_movement>().TalkingAction();
    }
    public void ClearSymbols(){
       // Debug.Log("Cleared Symbols");
        bubbleSymbols.Clear();
        xPosition = 0;
        displayDuration = 5f;
        bubbleDisplay.EndDisplay();
        foreach (Transform child in bubbleCanvas.gameObject.transform)
        {
            Destroy(child.gameObject);
        }
    }
    public void InjectSymbols(List<Symbol> inSymbols){
        Debug.Log("Injected Symbols");
        bubbleSymbols.Clear();
        bubbleSymbols = new List<Symbol>(inSymbols);
        DisplaySymbols();
    }
    public void InjectSymbols(Symbol inSymbol){
        Debug.Log("Injected Symbols");
        bubbleSymbols.Clear();
        Symbol symbolToAdd = inSymbol;
        bubbleSymbols.Add(symbolToAdd);
        DisplaySymbols();
    }
    //Display Time Coroutine

    void DisplayTimer(){
        ClearSymbols();
    }
}