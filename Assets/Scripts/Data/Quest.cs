﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;

public class Quest : MonoBehaviour
{
    //


    // Quest Type //--------------------------------------------------------------
    public enum QuestType
    {

        BringSomthingToNPC,
        BringSomthingToPlace,
        BringToRocket

    }


    //[Header("Name of Quest")]
    public string QuestName;

    public QuestType questType;

    // Player  //---------------------------------------------------------------

    GameObject Player;




    // NPC  //---------------------------------------------------------------
    [Header("Quest giver")]
    public GameObject NPC;


    // Quest Object //--------------------------------------------------------------

 
       
        [Header("Quest Objects")]
        

        public GameObject QuestObject;
        public GameObject QuestTarget;
        [Space]
        public GameObject Obsticle;


   

    // Quest Range //---------------------------------------------------------------
    
    [Header("Quest Range")]
    float dist;

    
    [Header("Spawn Object")]
    

    public Transform savePos;
    
    public List<GameObject> SpawnObject;


    // Rocket //---------------------------------------------------------------
    
    [Header("Rocket Slots")]
    public List<Transform> RocketSlot;
    public List<bool> Part;
    public List<GameObject> LostParts;
    public List<GameObject> ParticleEffects;


    // Quest //---------------------------------------------------------------------


    [Header("Next Quest")]
    public Quest NewQuest;

    
    [Header("Quest")]

   // [HideInInspector]
    public bool QuestActive = false;
    //[HideInInspector]
    public bool QuestFinished = false;
    [Range(min:1,max:50)]
    public float DropeRange;
    public bool GizmoIsActive = true;
    [Space] 
    public UnityEvent Quest_Solved;
    public UnityEvent Quest_Event;


    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");

    }
    // Update is called once per frame
   
    void Update()
    {  
        SolveDetection();
        QuestEvent();
    }


    void SolveDetection()
    {

        if (questType == QuestType.BringSomthingToNPC)
        {
            if (QuestObject != null)
            {

                dist = Vector3.Distance(NPC.gameObject.transform.position, QuestObject.gameObject.transform.position);
                Debug.DrawLine(NPC.gameObject.transform.position, QuestObject.gameObject.transform.position, Color.blue);

                if (QuestActive)
                {
                    if (dist <= DropeRange && QuestObject.transform.parent == null)
                    {
                        Debug.Log("finishQuest");
                        finishQuest();
                    }
                }

            }
        }

        else if (questType == QuestType.BringSomthingToPlace)
        {
            if (QuestObject != null)
            {
                dist = Vector3.Distance(QuestTarget.transform.position, QuestObject.gameObject.transform.position);
                if (QuestTarget != null)
                {
                    Debug.DrawLine(gameObject.transform.position, QuestTarget.gameObject.transform.position, Color.green);
                }
                if (QuestActive)
                {
                    if (dist <= DropeRange)
                    {
                        finishQuest();
                        Debug.Log("Quest Finished");
                    }
                }

            }
        }
        else if (questType == QuestType.BringToRocket)
        {


            if (Part[0])
            {
                ParticleEffects[0].SetActive(false);
            }
            if (Part[1])
            {
                ParticleEffects[1].SetActive(false);
            }
            if (Part[2])
            {
                ParticleEffects[2].SetActive(false);
            }



            foreach (Interactable IN in Player.GetComponentsInChildren<Interactable>())
            {
                if (IN.gameObject.CompareTag("Resource") && IN.gameObject.GetComponent<Resource>().resourceTyp == Resource.ResourceTyp.RocketPart)
                {
                    QuestObject = IN.gameObject;
                }
            }


            if (QuestObject == LostParts[0])
            {
                dist = Vector3.Distance(RocketSlot[0].position, QuestObject.gameObject.transform.position);
                Debug.DrawLine(RocketSlot[0].position, QuestObject.gameObject.transform.position, Color.yellow);
            }
            else if (QuestObject == LostParts[1])
            {
                dist = Vector3.Distance(RocketSlot[1].position, QuestObject.gameObject.transform.position);
                Debug.DrawLine(RocketSlot[1].position, QuestObject.gameObject.transform.position, Color.yellow);
            }
            else if (QuestObject == LostParts[2])
            {
                dist = Vector3.Distance(RocketSlot[2].position, QuestObject.gameObject.transform.position);
                Debug.DrawLine(RocketSlot[2].position, QuestObject.gameObject.transform.position, Color.yellow);
            }


            if (QuestActive)
            {
                if (QuestObject != null)
                {
                    if (dist <= DropeRange && QuestObject.transform.parent == null && QuestActive)
                    {

                        if (Part[0] && Part[1] && Part[2])
                        {
                            
                            finishQuest();
                        }
                        else
                        {
                            Quest_Solved.Invoke();
                        }
                    }
                }
            }

        }
    }


    public void Test()
    {
        Debug.Log("Quest_finished");
    }



    public void QuestEvent()
    {

        if (!QuestActive && QuestFinished)
        {

            Quest_Event.Invoke();

        }

    }

    public void BuildBrige()
    {
            QuestObject.transform.position = savePos.transform.position;
            QuestObject.transform.localRotation = savePos.transform.localRotation;
            QuestObject.GetComponent<Rigidbody>().useGravity = false;
            QuestObject.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
            QuestObject.GetComponent<Rigidbody>().freezeRotation = true;
            QuestObject.GetComponent<SphereCollider>().enabled = false;
            QuestObject.GetComponent<CapsuleCollider>().enabled = false;
            QuestObject.gameObject.tag = "Deco";
            QuestObject.GetComponent<Interactable>().enabled = false;
    }

    public void CarveStatue()
    {
        if (GameObject.FindGameObjectWithTag("Statue"))
        {
            savePos = QuestObject.transform;
            Destroy(QuestObject.gameObject);


            Instantiate(SpawnObject[0], savePos.transform.position, Quaternion.identity);
        }
    }

    public void GiveFood()
    {
        if (!GameObject.FindGameObjectWithTag("Food"))
        {
            Instantiate(SpawnObject[0], savePos.transform.position, Quaternion.identity);
        }
    }

    public void RepareRocket()
    {





        if (QuestObject != null) {

            if (QuestObject == LostParts[0])
            {

                QuestObject.transform.position = RocketSlot[0].position;
                QuestObject.transform.SetParent(RocketSlot[0].transform);

                QuestObject.GetComponent<BoxCollider>().enabled = false;
                QuestObject.GetComponent<SphereCollider>().enabled = false;

                QuestObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                QuestObject.GetComponent<Rigidbody>().useGravity = false;
                QuestObject.GetComponent<Rigidbody>().freezeRotation = true;

              QuestObject.transform.rotation = RocketSlot[0].rotation;

                Part[0] = true;

                
                Debug.Log("Part 1");

            }
            else if (QuestObject == LostParts[1])
            {

                QuestObject.transform.position = RocketSlot[1].position;
                QuestObject.transform.SetParent(RocketSlot[1].transform);
                QuestObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                QuestObject.GetComponent<MeshCollider>().enabled = false;
                QuestObject.GetComponent<SphereCollider>().enabled = false;
                QuestObject.GetComponent<Rigidbody>().useGravity = false;
                QuestObject.GetComponent<Rigidbody>().freezeRotation = true;
                Part[1] = true;

                QuestObject.transform.rotation = RocketSlot[1].rotation;
                Debug.Log("Part 2");
            }
            else if (QuestObject == LostParts[2])
            {
                QuestObject.transform.position = RocketSlot[2].position;
                QuestObject.transform.SetParent(RocketSlot[2].transform);
                QuestObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                QuestObject.GetComponent<BoxCollider>().enabled = false;
                QuestObject.GetComponent<SphereCollider>().enabled = false;
                QuestObject.GetComponent<Rigidbody>().useGravity = false;
                QuestObject.GetComponent<Rigidbody>().freezeRotation = true;
                Part[2] = true;

                QuestObject.transform.rotation = RocketSlot[2].rotation;
                Debug.Log("Part 3");
            }


        }
        else if (questType == QuestType.BringToRocket && (Player.GetComponent<Pickup>().PickUpObject == LostParts[0] || Player.GetComponent<Pickup>().PickUpObject == LostParts[1] || Player.GetComponent<Pickup>().PickUpObject == LostParts[2] ))
        {

            QuestObject = Player.GetComponent<Pickup>().PickUpObject;


        }
       

    }

    public void finishQuest()
    {
        QuestActive = false;
        QuestFinished = true;
        Quest_Solved.Invoke();
        if(NewQuest != null){
            NewQuest.SetQuestActive();
        }
     
    }


    public void SetQuestActive()
    {
        QuestActive = true;
    }

    private void OnDrawGizmos()
    {
        GameObject QH = GameObject.Find("Quest_Handler");

        if (transform.parent != QH)
        {
            gameObject.transform.SetParent(QH.transform);
        }

        if (QuestName == "")
        {
            QuestName = "New Quest";
        }
        else
        {
            gameObject.name = QuestName;
        }

       //  Gizmos.DrawLine(RocketSlot[0].transform.position, Parts[0].transform.position);
       //  Gizmos.DrawLine(RocketSlot[1].transform.position, Parts[1].transform.position);
       //  Gizmos.DrawLine(RocketSlot[2].transform.position, Parts[2].transform.position);





        if (NPC != null)
        {
            if (GizmoIsActive)
            {

                Gizmos.color = Color.green;

                Gizmos.DrawWireSphere(NPC.transform.position, DropeRange);
            }


            

            if (QH.transform.childCount != 0)
            {
                float i = gameObject.transform.GetSiblingIndex();
                // Debug.Log("Index Child: " + i);
                gameObject.transform.position = new Vector3(NPC.transform.position.x, NPC.transform.position.y + 1 - i / 3, NPC.transform.position.z);
            }
        }

        if (NewQuest != null)
        {
            Gizmos.color = Color.black;
            Gizmos.DrawLine(gameObject.transform.position,NewQuest.transform.position);
        }

        if (QuestObject != null)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(gameObject.transform.position, QuestObject.transform.position);
        }

        if (QuestTarget != null)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(gameObject.transform.position, QuestTarget.transform.position);
        }


    }

}