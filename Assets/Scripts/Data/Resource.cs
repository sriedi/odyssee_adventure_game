﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class Resource : MonoBehaviour
{

    public enum ResourceTyp
    {
        Wood,
        Turnip,
        Ston,
        Axe,
        Statue,
        RocketPart,
        Wing

    };
  public ResourceTyp resourceTyp;

}
