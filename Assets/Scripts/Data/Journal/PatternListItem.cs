﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class PatternListItem : MonoBehaviour, IPointerClickHandler
{
    public Pattern itemPattern;
    public Sprite itemSprite;
    public GameObject selectedSymbolEntry;
    public GameObject patternListObj;
    bool isSetToSymbol = false;
    bool isSelectActive = false;
    // Start is called before the first frame update
    private void Start() {
        patternListObj = gameObject.transform.parent.gameObject;
    }
    public void createItem(Pattern inPattern){
        itemPattern = inPattern;
        itemSprite = inPattern.patternSprite;
        gameObject.GetComponentInChildren<Image>().sprite = itemSprite;
        patternListObj = gameObject.transform.parent.gameObject;
    }
    //On Click move the Object into the Symbol Entry
    private void Update() {
        if(gameObject.GetComponentInParent<PatternList>() != null){
        selectedSymbolEntry = gameObject.GetComponentInParent<PatternList>().selectedSymbolObj;
        }
    }
    //On Click on the Pattern
    public void OnPointerClick(PointerEventData eventData){
        //If a Symbol Entry is active and current Symbol Entry status is select symbol
        if(selectedSymbolEntry != null && !isSetToSymbol && selectedSymbolEntry.GetComponent<SymbolEntry>().currentStatus == SymbolEntry.EntryStatus.selectingPattern){
                Debug.Log("Move Pattern to Object");
                selectedSymbolEntry.GetComponent<SymbolEntry>().currentStatus = SymbolEntry.EntryStatus.patternSelected;
                moveIntoJournal();
        }
        //IF we dont have a selected symbol and the pattern is set return it.
        
        else if(!patternListObj.GetComponent<PatternList>().patternSelectActive && isSetToSymbol)
        {
            isSetToSymbol = false;
            gameObject.GetComponentInParent<SymbolEntry>().resetPatternInput();
            patternListObj.GetComponent<PatternList>().selectedSymbolObj = null;
            gameObject.transform.SetParent(patternListObj.transform);
            gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0,0,0);
            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2 (100,100);
        }
    }
    //Move the Selected Pattern into the Journal
    public void moveIntoJournal(){
        selectedSymbolEntry.transform.parent.gameObject.GetComponent<JournalContent>().patternSelectActive = false;
        GameObject entryPatternObj = selectedSymbolEntry.GetComponent<SymbolEntry>().patternObj;
        gameObject.transform.SetParent(entryPatternObj.transform);
        gameObject.GetComponent<RectTransform>().sizeDelta = entryPatternObj.GetComponent<RectTransform>().sizeDelta;
        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0,0,0);
        selectedSymbolEntry.GetComponent<SymbolEntry>().validatePatternInput(this);//Give the PatternListItemScript and Validate it
        isSetToSymbol = true;
        }
    public void removeFormJournal(){
        gameObject.transform.SetParent(patternListObj.transform);
    }
}
