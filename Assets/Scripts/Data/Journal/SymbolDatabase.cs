﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SymbolDatabase : MonoBehaviour
{
    public List<Symbol> solvedSymbols;
    DailogeMenuData dialogeUiObj;
    //public GameObject 
    // Start is called before the first frame update
    void Start()
    {
        if(!dialogeUiObj){
            dialogeUiObj = FindObjectOfType<DailogeMenuData>();
        }    
    }
    

    public void addSymbol(Symbol inSymbol){
       if(checkDoubles(inSymbol)){
            solvedSymbols.Add(inSymbol);
            dialogeUiObj.addSymbol(inSymbol);
        }
    }
    bool checkDoubles(Symbol inSymbol){
        foreach (Symbol symbol in solvedSymbols)
        {   
            if(symbol.Equals(inSymbol)){
                return false;
            }
        }
        return true;
    }

}
