﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SymbolEntry : MonoBehaviour
{
    public GameObject patternButton;
    public GameObject patternObj;
    public GameObject symbolSpriteObj;
    public GameObject wordObj;
    public GameObject inputField;
    public Symbol entrySymbol;
    public bool patternSet=false;
    public enum EntryStatus
    {
        none,
        selectingPattern,
        patternSelected,
        patternSolved
    }
    public EntryStatus currentStatus = EntryStatus.none;
    PatternList patternListObject;
    [SerializeField]
    GameObject SymbolMenuObject;
    SymbolDatabase symbolDatabase;
    JournalContent journalContent;
    public PatternListItem setPatternItem;
    string currentInput = "word";
    
    // Start is called before the first frame update
    //Find the Pattern List in the Journal for Reference Use
    void Start()
    {
        patternListObject = FindObjectOfType<PatternList>();
        journalContent = FindObjectOfType<JournalContent>();
        if(SymbolMenuObject == null){
            SymbolMenuObject = GameObject.Find("DialogeUI");
        }
        if(!symbolDatabase){
            symbolDatabase = FindObjectOfType<SymbolDatabase>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckEntryState();
        validateInput();
    }
    //Check current Entrys State YES Rene it is a statemachine
    void CheckEntryState(){
        switch (currentStatus)
        {
            
            case EntryStatus.none:
                patternButton.SetActive(true);
            break;
            case EntryStatus.selectingPattern:
            patternButton.SetActive(false);
                if(!journalContent.patternSelectActive){
                    currentStatus = EntryStatus.none;
                }
            break;
            case EntryStatus.patternSelected:
            patternButton.SetActive(false);
            break;
            case EntryStatus.patternSolved:
            patternButton.SetActive(false);
            break;

        }
    }



    //Display the word
    void displayWord(){
        wordObj.SetActive(true);
        wordObj.GetComponent<TextMeshProUGUI>().text = entrySymbol.word[0];
    }
    //Delete the Set Pattern and display the Pattern
    void displayPattern(){
        patternObj.SetActive(true);
        patternObj.GetComponent<Image>().sprite = entrySymbol.pattern.patternSprite;
        if(setPatternItem != null){
            Destroy(setPatternItem.gameObject);
        }
    }

    //Called if the Journal Entry is created
    public void CreateEntry(){
        symbolSpriteObj.GetComponent<Image>().sprite = entrySymbol.symbolSprite;
        wordObj.GetComponent<TextMeshProUGUI>().text = entrySymbol.word[0];   
    }
    //Called form Pattern List Item if it is send to the Symbol Entry and Validates the Input
    public void validatePatternInput(PatternListItem inObj){
        setPatternItem = inObj;
        currentStatus = EntryStatus.patternSelected;
        validateInput();
    }
    public void resetPatternInput(){
        setPatternItem = null;
        currentStatus = EntryStatus.none;
    }
    public void validateTextInput(){
        currentInput = inputField.GetComponent<TMP_InputField>().text;
        validateInput();
    }
    //Called if there is a Input into the Textfield
    void validateInput(){
                
        if(currentInput !=  null && validateWord(currentInput)){
            confirmEntry();
        }

        if(setPatternItem != null && setPatternItem.itemPattern == entrySymbol.pattern){
            Debug.Log("Pattern Validated");
            confirmEntry();
        }

        if(symbolDatabase.solvedSymbols.Contains(entrySymbol)){
            confirmEntry();
        }
    }
    bool validateWord(string inString){
        foreach (string currentWord in entrySymbol.word)
        {
            if(inString.Equals(currentWord)){
                return true;
            }
        }
        return false;
    }

    //Called if the Button is clicked it sets the current active selected object for use in the Pattern List Script to move the pattenr object
    public void selectEntryInput(){
        if(!journalContent.patternSelectActive){
            journalContent.patternSelectActive = true;
            currentStatus = EntryStatus.selectingPattern;
            patternListObject.SetCurrentSelectedSymbol(gameObject);
            patternButton.SetActive(false);
        }
    }

//Confirm the Entry Selected
    public void confirmEntry(){
        if(currentStatus != EntryStatus.patternSolved){
            currentStatus = EntryStatus.patternSolved;
            inputField.SetActive(false);
            patternButton.SetActive(false);
            //Delete the Pattern form the list
            patternListObject.confirmEntry(entrySymbol.pattern);
            displayWord();
            displayPattern();
            symbolDatabase.addSymbol(entrySymbol);
            //Add the Symbol to the Database Component in Game Logic
            if(journalContent.solveEmitter != null){
                journalContent.solveEmitter.Play();
            }
        }
    }
}
