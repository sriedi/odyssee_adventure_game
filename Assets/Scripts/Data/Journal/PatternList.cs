﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PatternList : MonoBehaviour
{
    public List<Pattern> collectedPatterns;
    public List<Pattern> assignedPatterns;
    public GameObject selectedSymbolObj;
    public GameObject patternPrefab;
    public bool patternSelectActive = false;
    JournalContent journalContent;
    // Start is called before the first frame update
    void Start()
    {
        journalContent = FindObjectOfType<JournalContent>();
        foreach (Pattern pattern in collectedPatterns)
        {
            AddPattern(pattern);
        }
    }

    // Update is called once per frame
    void Update()
    {
        patternSelectActive = journalContent.patternSelectActive;
        //Sync with Journal Content to block if a patternselect is active.
    }
    public void AddPattern(Pattern inPattern){
        if(checkDoubles(inPattern)){
            collectedPatterns.Add(inPattern);
            GameObject instObj = Instantiate(patternPrefab,gameObject.transform,false);
            instObj.GetComponent<PatternListItem>().createItem(inPattern);
         }
    }
    public void SetCurrentSelectedSymbol(GameObject inSymbolObj){
        selectedSymbolObj = inSymbolObj;
    }
    //Call form Journal to confirm the enrty and removeit from the list an add it to the assinged list
    public void confirmEntry(Pattern inPattern){
        collectedPatterns.Remove(inPattern);
        PatternListItem[] patternItems = gameObject.GetComponentsInChildren<PatternListItem>();
        foreach (PatternListItem patternItem in patternItems)
        {
            if(patternItem.itemPattern == inPattern){
                Destroy(patternItem.gameObject);
            }
            
        }
    }
    bool checkDoubles(Pattern inPattern){
        foreach (Pattern pattern in collectedPatterns)
        {
            if(pattern.Equals(inPattern)){
                return false;
            }
        }
        return true;
    }
}
