﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class JournalContent : MonoBehaviour
{
    public GameObject entryPrefab;
    public List<Symbol> journalContentSymbols;
    public bool patternSelectActive = false;
    GameLogic gameLogic;
    public StudioEventEmitter discoveryEmitter;
    public StudioEventEmitter solveEmitter;
    // Start is called before the first frame update
    void Start()
    {
        gameLogic = FindObjectOfType<GameLogic>();
        StartJournal();
    }

    // Update is called once per frame
    void Update()
    {
        if(gameLogic.interfaceState != GameLogic.InterfaceState.Journal){
            patternSelectActive = false;
        }
    }    

    //Cretes The List on Start
    void StartJournal(){
        foreach (Symbol symbol in journalContentSymbols)
            {
               UpdateJournal(symbol);
            }
    }
    //Creates an Entry with given Symbol
    void UpdateJournal(Symbol inSymbol){
         GameObject instObj = Instantiate(entryPrefab,gameObject.transform,false);
        instObj.GetComponent<SymbolEntry>().entrySymbol = inSymbol;
        instObj.GetComponent<SymbolEntry>().CreateEntry();
    }
   //Add a Symbol to the Journal
    public void AddSymbol(Symbol inSymbol){
       if(checkDoubles(inSymbol)){
            journalContentSymbols.Add(inSymbol);
            UpdateJournal(inSymbol);
            if(discoveryEmitter != null){
                 discoveryEmitter.Play();
            }
        }
        
    }
    //Check if there is a double already in the journal
    bool checkDoubles(Symbol inSymbol){
         foreach (Symbol symbol in journalContentSymbols)
        {   
            if(symbol.Equals(inSymbol)){
                return false;
            }
        }
        return true;
    }
    

}
