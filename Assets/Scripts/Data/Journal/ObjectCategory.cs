﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCategory : MonoBehaviour
{
    public enum Category
    {
        rocketpart1,
        rocketpart2,
        rocketpart3,
        wood,
        axe,
        totem,
        food
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
