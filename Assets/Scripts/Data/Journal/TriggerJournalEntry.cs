﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerJournalEntry : MonoBehaviour
{
    // Start is called before the first frame update

    public Pattern discorverPattern;
    public List<Symbol> discoverSymbol;
    public bool isOnTrigger;
    public bool isOnMouseDown;

    [HideInInspector]
    public GameLogic gameLogic;
    //Find the Game Logic to Add the Pattern to the list of the Journal Object Referenced there
    void Start()
    {
        gameLogic = FindObjectOfType<GameLogic>();
    }

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")&&isOnTrigger){
            addEntryObj();
        }
    }
    //A simple MouseDown triggers
    private void OnMouseDown() {
        if(isOnMouseDown){
            addEntryObj();
        }
        
    }
    //This can be called external
    public void addEntryObj(){
        if(discorverPattern != null){
          //  Debug.Log("Added "+discorverPattern.name);
            FindObjectOfType<PatternList>().AddPattern(discorverPattern);
        }
        if(discoverSymbol != null){
            foreach (Symbol symbol in discoverSymbol)
            {
                gameLogic.journalUiObj.GetComponentInChildren<JournalContent>().AddSymbol(symbol); 
            }
            
        }
    }
}
