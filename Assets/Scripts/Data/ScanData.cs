﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class ScanData : MonoBehaviour
{
    public Sprite objectScanSprite;
    [TextArea]
    public string objectScanDescription;
    public float range = 1.5f;
    MessageUI messageSystem;
    PlayerNavMesh playerNavMesh;
    public GameObject droneObj;
    [SerializeField]
    UnityEvent onScanEvent;
    bool scanTriggered = false;

    // Start is called before the first frame update
    void Start()
    {
        if(!droneObj){
            droneObj = GameObject.FindGameObjectWithTag("Drone");
        }
        if(!messageSystem){
          messageSystem = FindObjectOfType<MessageUI>();
      }   
    }
    void Update() {
        playerNavMesh = FindObjectOfType<PlayerNavMesh>();
        if(droneObj.GetComponent<DroneController>().currentMoveState == MoveState.ScaningObjcets){
            if(CheckForScan()){
                if(!scanTriggered && droneObj.GetComponent<DroneController>().currentDroneTarget == gameObject){
                    scanTriggered = true;
                    Invoke("onScan",2.5f);
                    onScanEvent.Invoke();
                }
            }
        }
    }

//Is Executed if the Object is Scanned by the Drone
    public void onScan(){
        if(objectScanSprite != null){
            messageSystem.DisplayMessage(objectScanDescription,objectScanSprite);
        }
        else {
            messageSystem.DisplayMessage(objectScanDescription);
        }
    }

    //Is the Drone in the Defined float RANGE then return true.
    bool CheckForScan()
    {
        float dist = Vector3.Distance(droneObj.transform.position,gameObject.transform.position);
        //Debug.Log(dist);
        if( dist <= range){
            return true;
        }
        return false;
        
    }
}
