﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DistTrigger : MonoBehaviour
{
    public GameObject targetObj;

    public UnityEvent targetReached;
    public float testDist = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(targetObj != null){
        float dist = Vector3.Distance(targetObj.transform.position,gameObject.transform.position);
        if(dist <= testDist){
            TriggerEvents();
        }
        }
    }
    void TriggerEvents(){
        if(targetReached != null){
            targetReached.Invoke();
        }
    }
}
