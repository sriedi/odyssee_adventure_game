﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class EndOfVideo : MonoBehaviour
{
    VideoPlayer VP;
    VideoClip VC;

    // Start is called before the first frame update
    void Start()
    {
        VC = GetComponent<VideoPlayer>().clip;
        VP = GetComponent<VideoPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "Intro" && VP.time > 81)
        {
            SceneManager.LoadScene("World");
        }
        if(SceneManager.GetActiveScene().name == "Intro"){
            if(Input.GetKeyDown(KeyCode.Escape)){
                SceneManager.LoadScene("World");
            }
        }

        if (SceneManager.GetActiveScene().name == "Outtro" && VP.time > 28)
        {
            
            SceneManager.LoadScene("MainMenu");
        }
    }
    
}
