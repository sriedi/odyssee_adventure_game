﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClickInteraction : MonoBehaviour
{
    public UnityEvent mouseDown;
    public UnityEvent mouseUp;
    public UnityEvent mouseOver;
    // Start is called before the first frame update

    private void OnMouseDown() {
        if(mouseDown != null){
            mouseDown.Invoke();
        }
    }
    private void OnMouseUp() {
        if(mouseUp != null){
            mouseUp.Invoke();
        }
    }
    private void OnMouseOver() {
        if(mouseOver != null){
            mouseOver.Invoke();
        }
    }
}
