﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using FMODUnity;

      public enum MoveState
    {
            Idle,
            MoveToObject,
            MoveToPlayer,
            ScaningObjcets,

    }
public class DroneController : MonoBehaviour
{
    public GameObject playerObject;
    public GameObject droneTargetObj;
    public GameObject currentDroneTarget;
    public MoveState currentMoveState = MoveState.Idle;
    public float radiusToPlayer;
    public float radiusToObjects;
    public float turningSpeed;
    Animator droneAnimator;
    public StudioEventEmitter droneEmitterMove;
    public StudioEventEmitter droneOrderEmitter;
    public StudioEventEmitter droneScannEmitter;
    public StudioEventEmitter droneJournalEmitter;
    NavMeshAgent droneAgent;
    GameLogic gameLogic;

    // Start is called before the first frame update
    void Start()
    {
        if(!playerObject){
            playerObject = GameObject.FindGameObjectWithTag("Player");
        }
        
        if(!droneTargetObj){
            droneTargetObj = GameObject.Find("DroneTarget");
        }
        if(!gameLogic){
            gameLogic = FindObjectOfType<GameLogic>();
        }
        droneAnimator = gameObject.GetComponentInChildren<Animator>();
        droneAgent = gameObject.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        checkStates();
        AnimationHandler();
    }
    private void OnMouseUp() {
        Debug.Log("Clicked on Drone");
        if (droneJournalEmitter != null){
            droneJournalEmitter.Play();
        }
        gameLogic.LoadJournal();
    }
    public void checkStates(){
        switch (currentMoveState)
        {
            case MoveState.Idle :
            orbitPlayer();
            break;
            case MoveState.MoveToObject :
            scanCheck();
            break;
            case MoveState.MoveToPlayer :
            returnToPlayer();
            break;
            case MoveState.ScaningObjcets :
            break;
        }
    }
    //Select a Target as Drone Target and let it Calculate 
    public void orderPosition(GameObject inObject){
        currentMoveState = MoveState.MoveToObject;
        currentDroneTarget = inObject;
       
        Vector3 targetVectorCalc = new Vector3(0,0,0);
        if(inObject.GetComponent<ScanData>() != null){
            if(Vector3.Distance(inObject.transform.position,transform.position) > inObject.GetComponent<ScanData>().range)
                targetVectorCalc = CalculateDroneTarget(inObject.transform.position,transform.position,inObject.GetComponent<ScanData>().range - 0.3f);
            }
        else
        {
            if(Vector3.Distance(inObject.transform.position,transform.position) > radiusToObjects){
                targetVectorCalc = CalculateDroneTarget(inObject.transform.position,transform.position,radiusToObjects);
            }
        }
        if(targetVectorCalc != Vector3.zero){
        droneTargetObj.transform.position = targetVectorCalc;
        }
        droneTargetObj.transform.LookAt(inObject.transform.position,Vector3.up);
        if(droneOrderEmitter != null){
            droneOrderEmitter.Play();
        }
        if(droneAgent){
            droneAgent.SetDestination(droneTargetObj.transform.position);
        }
    }
    //Oribit Around the Player
    void orbitPlayer(){
        if( Vector3.Distance(playerObject.transform.position,droneTargetObj.transform.position) > radiusToPlayer){
            droneTargetObj.transform.position = CalculateDroneTarget(droneTargetObj.transform.position,playerObject.transform.position,radiusToPlayer);
            if(droneAgent){
            droneAgent.SetDestination(droneTargetObj.transform.position);
            }
        }
    }
    //Invoke if a Drone Scan is ordered
    void scanCheck(){
        float dist = droneAgent.remainingDistance;
        if(droneScannEmitter != null){
            droneScannEmitter.Play();
        }
        if(dist != Mathf.Infinity && droneAgent.pathStatus == NavMeshPathStatus.PathComplete && droneAgent.pathStatus != NavMeshPathStatus.PathInvalid){
            Invoke("returnToPlayer",5f);
            currentMoveState = MoveState.ScaningObjcets;
        }
    }
    //return to the Player
    void returnToPlayer(){
        if(droneScannEmitter != null){
            droneScannEmitter.Stop();
        }
        currentDroneTarget = playerObject;
        currentMoveState = MoveState.MoveToPlayer;
        droneTargetObj.transform.position = CalculateDroneTarget(playerObject.transform.position,transform.position,radiusToPlayer);
        if(droneAgent){
            droneAgent.SetDestination(droneTargetObj.transform.position);
        }
        if(droneAgent.remainingDistance <= radiusToPlayer){
            currentMoveState = MoveState.Idle;
        }
    }
    //Calculate the Position of the Drone Target Obj
    Vector3 CalculateDroneTarget(Vector3 inCurrentPosition, Vector3 inTargetObjPosition,float inRadius){
        Vector3 distVector = inTargetObjPosition - inCurrentPosition;
        Vector3 distVectorNorm = Vector3.Normalize(distVector);
        Vector3 returnPosition = inCurrentPosition + inRadius*distVectorNorm;
        return returnPosition;
    }

    void AnimationHandler(){
        
        float velocity = droneAgent.velocity.magnitude/droneAgent.speed;
        //Debug.Log("DroneSpeed is "+velocity);
        if(velocity < 0.6f){//Use Velocity like NPC 
            droneAnimator.SetTrigger("idle");
        }
        if(velocity > 0.6f){
            droneAnimator.SetTrigger("isMoveing");
        }
        droneEmitterMove.EventInstance.setParameterByName("DroneMovement",velocity,false);
    }

}
