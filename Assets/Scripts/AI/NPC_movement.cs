﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class NPC_movement : MonoBehaviour
{
    //  Movment  //----------------------------------------------------------------------------------
    public enum MovmentMode
    {
        Idle,
        GoTo,
        Partol,
        follow,
        RunAWay
    }

    public enum Position
    {
         GoTo,
         Home

    }

   

        [Header("Current Stat of Movement")]
    public MovmentMode movmentMode;
    public Position position;
    NavMeshAgent NPCnavMeshAgent;

    //  Targets  //----------------------------------------------------------------------------------


    [Header("Go to or Follow Target")]
    GameObject MoveToTarget;
    public GameObject GoToPos;
    public GameObject Home;

    public bool EndOfMovment = false;

    public GameObject FollowTarget;
    float dist;
    bool Goto;

    public GameObject GrabPos;

    public GameObject PickupObject;
    public bool NPCCarry;




    //  Patrol  //---------------------------------------------------------------------------------- 

    public enum TypOfPartol
    {
        Circle,
        PingPong
    }

    [Header("Patrol Mode")]
    public TypOfPartol typOfPatrol;
    public List<Transform> WayPoints;
    Transform currentTarget;
    Transform lastTarget;
    int i;
    bool Direction;


    //  Run a Way  //---------------------------------------------------------------------------------- 

    [Header("Run a Way")]
    public GameObject ScareCrow;
    Vector3 MoveDirection;
    float RunDistance = 0.5f;

    [Range(min: 1, max: 10)]
    public float ScareRange;
    public bool GizmoIsActive;


    //  Pickup  //---------------------------------------------------------------------------------- 

    [Header("Pickup")]
    public GameObject PickupItem;

    [Range(min: 1, max: 10)]
    public float DropDist;

   public GameObject DropPos;


    public List<GameObject> inYourHands;

   

    //  Events  //---------------------------------------------------------------------------------- 

    public UnityEvent arrivedDesination;


    //  Animation  //---------------------------------------------------------------------------------- 
    [Header("Animation")]
    public Animator animator;
    public NavMeshAgent navMeshAgent;
    [HideInInspector]
    public bool Panic;
    public bool Pray;
    public bool Talking;
    public bool Agree;
    public bool Disagree;



    //  look at Target  //---------------------------------------------------------------------------------- 
    [Header("LookAt")]
    public GameObject StartTarget;
    GameObject Target;
    GameObject PlayerTarget;

    [Range(min: 5, max: 20)]
    public float LookAtDist = 10;

    [Range(min: 0.75f, max: 0.99f)]
    public float Turningspeed = 0.9f;
      

    private void Start()
    {

        
        PlayerTarget = GameObject.FindGameObjectWithTag("Player");

        if (GetComponent<AllowPickUp>())
        {
   
        }
        if (!GetComponent<NavMeshAgent>())
        {
            gameObject.AddComponent<NavMeshAgent>();
        }

        NPCnavMeshAgent = GetComponent<NavMeshAgent>();
    }

   /* private void FixedUpdate()
    {

        Ray rayH = new Ray(navMeshAgent.transform.position, navMeshAgent.transform.forward);

        Debug.DrawLine(navMeshAgent.transform.position, rayH.GetPoint(DropDist), Color.red);
        Ray rayV = new Ray(rayH.GetPoint(DropDist), Vector3.down);

        Debug.DrawRay(rayH.GetPoint(DropDist), Vector3.down, Color.red);


        RaycastHit hitV;

        if (Physics.Raycast(rayV, out hitV))
        {
            DropPos = new Vector3(hitV.point.x, hitV.point.y + 3, hitV.point.z);


        }
    }   */



    private void Update()
    {

        if (inYourHands.Count == 0)
        {
            NPCCarry = false;
        }
        else
        {
            NPCCarry = true;
        }



        NPCMovemt();
        Animation();
        TargetDetection();




        


        if (movmentMode == MovmentMode.Idle)
        {
            navMeshAgent.updateRotation = false;
            LookAtPlayer();

        }
        else
        {
            navMeshAgent.updateRotation = true;
        }
        // Debug.Log(Panic);
    }


    public void TalkingAction()
    {
        Talking = true;
    }
    public void TalkingOver(){
        Talking = false;
    }

    public void PrayActive()
    {
        Pray = true;
    }

    public void PrayOver()
    {
        Pray = false;
    }

    public void PanicActive()
    {
        Panic = true;
    }

    public void PanicOver()
    {
        Panic = false;
    }
    public void AgreeAction(){
        Agree = true;
    }
    public void AgreeOver(){
        Agree = false;
    }
    public void DisagreeAction(){
        Disagree = true;
    }
    public void DisagreeOver(){
        Disagree = false;
    }

    public void End()
    {

        if (position == Position.Home)
        {
            EndOfMovment = true;
        }
    }

    public void MoveHome()
    {
        position = Position.Home;
    }

    public void MoveGoto()
    {
        position = Position.GoTo;
    }

    public void Idle()
    {
        movmentMode = MovmentMode.Idle;
    }

    public void GoTo()
    {
        movmentMode = MovmentMode.GoTo;
    }

    public void follow()
    {
        movmentMode = MovmentMode.follow;
    }

    public void Partol()
    {
        movmentMode = MovmentMode.Partol;
    }

    public void RunAWay()
    {
        movmentMode = MovmentMode.RunAWay;
    }

    public void NPCMovemt()
    {

           switch (position)
        {
            case Position.GoTo:

                MoveToTarget = GoToPos;

                break;

            case Position.Home:

                MoveToTarget = Home;

                break;

        }


        switch (movmentMode)
        {

            case MovmentMode.Idle:



                break;

            case MovmentMode.GoTo:

                NPCnavMeshAgent.SetDestination(MoveToTarget.transform.position);

                Panic = false;

                dist = Vector3.Distance(gameObject.transform.position, MoveToTarget.transform.position);

                if (dist < 5)
                {
                    if (position == Position.Home)
                    {
                        EndOfMovment = true;
                       
                    }
                    arrivedDesination.Invoke();

                    if (EndOfMovment)
                    {
                        Idle();
                        position = Position.GoTo;
                        EndOfMovment = false;
                    }

                }


                break;

            case MovmentMode.follow:


                Target = FollowTarget;

               Panic = false;

                dist = Vector3.Distance(gameObject.transform.position, FollowTarget.transform.position);
                if (dist < 3)
                {
                    NPCnavMeshAgent.stoppingDistance = 3f;
                }
                else
                {
                    if (FollowTarget != null)
                    {
                        NPCnavMeshAgent.SetDestination(FollowTarget.transform.position);
                    }
                }

                break;

            case MovmentMode.Partol:

                PatrolManager();
                Panic = false;

                break;

            case MovmentMode.RunAWay:

                Panic = true;

                GizmoIsActive = true;
                var MoveDirection = gameObject.transform.position - ScareCrow.transform.position;

                Debug.DrawRay(gameObject.transform.position, MoveDirection, Color.red);


                Ray rayH = new Ray(gameObject.transform.position, MoveDirection);

                Debug.DrawRay(rayH.GetPoint(RunDistance), Vector3.down, Color.red);
                Ray rayV = new Ray(rayH.GetPoint(RunDistance), MoveDirection);
                RaycastHit hitV;

                if (Physics.Raycast(rayV, out hitV))
                {
                    float dist = Vector3.Distance(gameObject.transform.position, ScareCrow.transform.position);

                    if (dist < ScareRange)
                    {

                        NPCnavMeshAgent.SetDestination(hitV.point);
                    }
                }



                break;

        }
    }

    public void PatrolManager()
    {
        if (typOfPatrol == TypOfPartol.PingPong)
        {


            if (WayPoints.Count != 0)
            {


                // Debug.Log(i);
                NPCnavMeshAgent.SetDestination(WayPoints[i].transform.position);

                float dist = Vector3.Distance(gameObject.transform.position, WayPoints[i].transform.position);

                if (dist <= 1)
                {

                    if (Direction)
                    {
                        ++i;
                    }

                    else

                    {
                        --i;
                    }
                }


            }


            if (WayPoints.Count - 1 == i)
            {

                Direction = false;

            }

            if (i == 0)
            {
                Direction = true;
            }

        }

        if (typOfPatrol == TypOfPartol.Circle)

        {
            if (WayPoints.Count != 0)
            {
                if (WayPoints[i] != null)
                {
                    NPCnavMeshAgent.SetDestination(WayPoints[i].transform.position);
                }

                float dist = Vector3.Distance(gameObject.transform.position, WayPoints[i].transform.position);

                if (dist <= 1)
                {
                    if (WayPoints.Count - 1 != i)
                    {
                        ++i;
                    }

                    else

                    {

                        i = 0;

                    }

                }
            }
        }

    }


    private void OnDrawGizmos()
    {

        if (GizmoIsActive)

        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(gameObject.transform.position, ScareRange);
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(gameObject.transform.position, LookAtDist);

        }


    }

    public void Animation()
    {
       
            if (navMeshAgent.velocity != new Vector3(0, 0, 0))
            {
            if (!Pray)
            {
                animator.SetBool("Movment", true);
            }

            }
            else
            {
                animator.SetBool("Movment", false);
            }


            if (Panic && !animator.GetBool("Panic"))
            {
                animator.SetBool("Panic", true);

            }
            else
            {
                animator.SetBool("Panic", false);

            }


            if (Pray)
            {
                animator.SetBool("Pray", true);

            }
            else
            {
                animator.SetBool("Pray", false);

            }



        if (NPCCarry)
        {
            animator.SetBool("Carry", true);
        }
        else
        {
            animator.SetBool("Carry", false);
        }

        if (Talking)
        {
            animator.SetTrigger("Talk");
            Pray = false;
            TalkingOver();
        }
        if(Disagree)
        {
            animator.SetTrigger("No");
            Pray = false;
            DisagreeOver();
        }
        if(Agree)
        {
            animator.SetTrigger("Yes");
            Pray = false;
            AgreeOver();
        }

        
    }

    public GameObject TargetDetection()
    {
        dist = Vector3.Distance(PlayerTarget.transform.position, gameObject.transform.position);

        if (MovmentMode.Idle == movmentMode)
        {
            if (dist <= LookAtDist)
            {

                Target = PlayerTarget;

            }
            else
            {
                Target = StartTarget;

            }

        }
     /*
        if (GetComponent<AllowPickUp>()) {
            if (MovmentMode.follow == movmentMode && GetComponent<AllowPickUp>().AllowObject.Contains(GameObject.FindGameObjectWithTag("Player").GetComponent<Pickup>().PickUpObject))
            {
                Target = PlayerTarget;
            }



            if (NPCCarry)
            {
                Target = Home;
            }
               
        }
    
       */
            return (Target);
    }



    public void LookAtPlayer()
    {

        if (Target != null)
        {
            if (!Pray)

            {
                Vector3 lookVecor = Target.transform.position - navMeshAgent.transform.position;
                Vector3 lerpLookDir = Vector3.Lerp(lookVecor, navMeshAgent.transform.forward, Turningspeed);
                transform.LookAt(navMeshAgent.transform.position + lerpLookDir);

            }
        }

    }

    public void AttackPlayer()
    {

        if (NPCCarry)
        {
            navMeshAgent.speed = 4;
        }
        else
        {
            navMeshAgent.speed = 7;
        }

     


        //Debug.Log(GameObject.FindGameObjectWithTag("Player").transform.GetComponentInChildren<Resource>().gameObject);
        if (GameObject.FindGameObjectWithTag("Player").transform.GetComponentInChildren<Resource>())
        {
   

            if (GetComponent<AllowPickUp>() && GetComponent<AllowPickUp>().AllowObject.Contains(PlayerTarget.transform.GetComponentInChildren<Resource>().gameObject))
            {
                PickupObject = PlayerTarget.transform.GetComponentInChildren<Resource>().gameObject;

                  //  FollowTarget = PickupObject;
                    GetComponent<NPC_movement>().movmentMode = MovmentMode.follow;
            }


             float dist = Vector3.Distance(gameObject.transform.position, GameObject.FindGameObjectWithTag("Player").transform.position);

                if (dist <= 3 && PickupObject.transform.parent == true && inYourHands.Count == 0)
                {

                    GameObject.FindGameObjectWithTag("Player").GetComponent<Pickup>().Drop();
                    NPCPickUP();
                    GetComponent<NPC_movement>().movmentMode = MovmentMode.GoTo;

                }
            }
        
    }



  public void NPCPickUP()
    {
         
        PickupObject.transform.rotation = new Quaternion(GrabPos.transform.rotation.x, GrabPos.transform.rotation.y, GrabPos.transform.rotation.z, GrabPos.transform.rotation.w);
        PickupObject.transform.position = new Vector3(GrabPos.transform.position.x, GrabPos.transform.position.y, GrabPos.transform.position.z);
        PickupObject.transform.SetParent(GrabPos.transform);
        PickupObject.GetComponent<Rigidbody>().isKinematic = true;
        PickupObject.GetComponent<SphereCollider>().enabled = false;

        if (PickupObject.GetComponent<BoxCollider>())
        {
            PickupObject.GetComponent<BoxCollider>().enabled = false;
        }

        if (PickupObject.GetComponent<NavMeshObstacle>())
        {
            PickupObject.GetComponent<NavMeshObstacle>().enabled = false;
        }
        
        inYourHands.Add(PickupObject.gameObject);

    }

  public void NPCDrop()
    {
        if (inYourHands.Count != 0) {

            inYourHands[0].transform.position = DropPos.transform.position;
            inYourHands[0].transform.rotation = DropPos.transform.rotation;
            inYourHands[0].transform.parent = null;
            inYourHands[0].GetComponent<Rigidbody>().isKinematic = false;
            inYourHands[0].GetComponent<SphereCollider>().enabled = true;

            if (PickupObject.GetComponent<BoxCollider>())
            {
                PickupObject.GetComponent<BoxCollider>().enabled = true;
            }
            if (PickupObject.GetComponent<NavMeshObstacle>())
            {
                PickupObject.GetComponent<NavMeshObstacle>().enabled = true;
            }

            Debug.Log("NPC_Drop");
            inYourHands.Remove(inYourHands[0]);
            //PickupObject = null;
            //Debug.Log(inYourHands.Count);
        }
   
    }





}
