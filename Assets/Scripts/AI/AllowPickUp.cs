﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class AllowPickUp : MonoBehaviour
{

    public GameObject Player;


    public bool AllowPickup;
    public List<GameObject> AllowObject;
    public List<GameObject> EffectetNPC;

    Vector3 CurrentPos;

    public UnityEvent NotAllowed;
    public UnityEvent BackAtPlace;

    [HideInInspector]
    public float AngerCount = 0;
    public float AngerMaxCount = 0;


    public enum AllowTyp
    {
        Object,
        Zone
    }
    public AllowTyp allowTyp;


    private void Start()
    {

        foreach (GameObject go in AllowObject)
        {
            if (go) {

                CurrentPos = go.transform.position;
            }
        }


        Player = GameObject.FindGameObjectWithTag("Player");
    }



    private void Update()
    {
        TryPickup();
    }



    void BringBack()
    {
        foreach (GameObject go in AllowObject)
        {

            float dist = Vector3.Distance(go.transform.position, GetComponent<NPC_movement>().DropPos.transform.position);

            if (dist < 1)
            {
                BackAtPlace.Invoke();
            }
        }
    }





    void TryPickup()
    {

        switch (allowTyp)
        {
            case AllowTyp.Object:

                if (!AllowPickup)
                {
                    if (AllowObject != null) {

                        foreach (GameObject go in AllowObject)
                        {

                          
                                if (Player.GetComponent<Pickup>().PickUpObject == go && Player.GetComponent<Pickup>().Carry)
                                {
                                    if (EffectetNPC.Count != 0)
                                    {
                                        foreach (GameObject ENPC in EffectetNPC)
                                        {
                                            ENPC.GetComponent<NPC_movement>().PanicActive();


                                        }
                                    }

                                    if (AngerCount != AngerMaxCount)
                                    {
                                        AngerCount++;
                                        NotAllowed.Invoke();
                                    gameObject.GetComponent<NPC_movement>().DisagreeAction();
                                    GameObject.FindGameObjectWithTag("Player").GetComponent<Pickup>().DropPos = CurrentPos;
                                    GameObject.FindGameObjectWithTag("Player").GetComponent<Pickup>().Drop();
                                    }
                                    else
                                    {

                                        GetComponent<NPC_movement>().AttackPlayer();

                                    }
                                }

                                else
                                {
                                    if (EffectetNPC.Count != 0)
                                    {
                                        foreach (GameObject ENPC in EffectetNPC)
                                        {
                                            ENPC.GetComponent<NPC_movement>().PanicOver();


                                        }
                                    }
                                }
                           
                          

                        }
                    }
                }


                break;

            case AllowTyp.Zone:




                break;

        }

    }


   public void DoAllow()
    {
        AllowPickup = true;
    }




}
